﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RainbowColor : MonoBehaviour
{
    public float shiftRate = 0.5f;
    public Text text;
    public Image image;

    void OnEnable()
    {
        if(text)
            StartCoroutine(shiftTextColor(Utilities.RandomColor(Color.black, Color.white)));
        if (image)
            StartCoroutine(shiftImageColor(Utilities.RandomColor(Color.black, Color.white)));

    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    IEnumerator shiftImageColor(Color target)
    {
        float time = 0f;
        while(time < shiftRate)
        {
            image.color = Color.Lerp(image.color, target, Time.deltaTime/shiftRate);
            time += Time.deltaTime;
            yield return null;
        }
        StartCoroutine(shiftImageColor(Utilities.RandomColor(Color.black, Color.white)));
    }

    IEnumerator shiftTextColor(Color target)
    {
        float time = 0f;
        while (time < shiftRate)
        {
            text.color = Color.Lerp(text.color, target, Time.deltaTime / shiftRate);
            time += Time.deltaTime;
            yield return null;
        }
        StartCoroutine(shiftTextColor(Utilities.RandomColor(Color.black, Color.white)));
    }
}
