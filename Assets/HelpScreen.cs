﻿using UnityEngine;
using System.Collections;

public class HelpScreen : MonoBehaviour {

    public Transform[] Screens;
    public int ScreenIndex = 0;
    public Vector3 StartPosition;
    public Vector3[] StartScreenPositions;
    void Start()
    {
        StartPosition = Screens[0].transform.position;
        StartScreenPositions = new Vector3[Screens.Length];
        for (int i = 0; i < Screens.Length; i++)
            StartScreenPositions[i] = Screens[i].transform.position;
    }

	public void rotateScreen()
    {
        

        ScreenIndex = ((ScreenIndex+1) % Screens.Length);
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            rotateScreen();
        }
            Vector3 targetdif = (StartScreenPositions[ScreenIndex] - StartPosition);
        for (int i = 0; i < Screens.Length; i++)
        {
            Screens[i].transform.position = Vector3.Lerp(Screens[i].transform.position, StartScreenPositions[i]-targetdif, Time.deltaTime*5f) ;


        }

    }
}
