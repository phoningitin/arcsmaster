﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using ForieroEngine.Extensions;

#if UNITY_EDITOR
using System.IO;
using UnityEditor;
#endif

namespace ForieroEngine
{
	public static class FResources
	{
		public static T Instance <T> (string name, string assetPath = "Resources") where T : ScriptableObject
		{
			T instance = Resources.Load<T> (name);

			if (instance == null) {
				if (Application.isPlaying) {
					Debug.LogError (name + " did not existed, creating one in memory!");
				}

				instance = ScriptableObject.CreateInstance<T> ();

				#if UNITY_EDITOR
				// Get resources folder path
				var resourcesPath = "Assets/" + assetPath + "/" + name + ".asset";

				var resourceDirectory = Application.dataPath + "/" + assetPath;

				// Create directory if it doesn't exist
				System.IO.Directory.CreateDirectory (resourceDirectory);

				// Save instance if in editor
				AssetDatabase.CreateAsset (instance, resourcesPath);

				AssetDatabase.SaveAssets ();
				#endif
			}

			return instance;
		}

		#if UNITY_EDITOR
		public static T EditorInstance <T> (string name, string assetPath = "Assets/Editor", bool newInstance = false) where T : ScriptableObject
		{
			T instance = AssetDatabase.LoadAssetAtPath <T> (assetPath + "/" + name + ".asset");

			if (newInstance || instance == null) {
				if (Application.isPlaying) {
					Debug.LogError (name + " did not existed, creating one in memory!");
				}

				instance = ScriptableObject.CreateInstance<T> ();

				// Get resources folder path
				var resourcesPath = assetPath + "/" + name + ".asset";

				var resourceDirectory = Path.Combine (Directory.GetCurrentDirectory (), assetPath.FixOSPath ());

				// Create directory if it doesn't exist
				System.IO.Directory.CreateDirectory (resourceDirectory);

				// Save instance if in editor
				AssetDatabase.CreateAsset (instance, AssetDatabase.GenerateUniqueAssetPath (resourcesPath));

				AssetDatabase.SaveAssets ();
			}

			return instance;
		}
		#endif
	}
}