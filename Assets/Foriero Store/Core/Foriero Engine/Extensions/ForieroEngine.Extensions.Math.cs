﻿using UnityEngine;
using System.Collections;

namespace ForieroEngine.Extensions
{
	public static partial class ForieroEngineExtensions
	{
		public static int GreatestCommonDivider (this int a, int b)
		{
			int Remainder;

			while (b != 0) {
				Remainder = a % b;
				a = b;
				b = Remainder;
			}

			return a;
		}
	}
}
