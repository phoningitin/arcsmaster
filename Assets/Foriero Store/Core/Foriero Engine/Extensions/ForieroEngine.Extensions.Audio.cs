﻿using UnityEngine;
using System.Collections;

namespace ForieroEngine.Extensions
{
	public static partial class ForieroEngineExtensions
	{
	
		public static IEnumerator PlayOneShotDelayed (this AudioSource anAudioSource, AudioClip anAudioClip, float aDelay)
		{
			while (aDelay > 0) {
				yield return null;
				aDelay -= Time.deltaTime;
			}
			anAudioSource.PlayOneShot (anAudioClip);	
		}

	}
}
