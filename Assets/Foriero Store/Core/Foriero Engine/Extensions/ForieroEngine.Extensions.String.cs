﻿using UnityEngine;
using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace ForieroEngine.Extensions
{
	public static partial class ForieroEngineExtensions
	{
		/// <summary>
		/// Remove HTML from string with Regex.
		/// </summary>
		public static string StripTagsRegex (this string source)
		{
			return Regex.Replace (source, "<.*?>", string.Empty);
		}

		/// <summary>
		/// Compiled regular expression for performance.
		/// </summary>
		//static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
		
		/// <summary>
		/// Remove HTML from string with compiled Regex.
		/// </summary>
		//public static string StripTagsRegexCompiled(this string source)
		//{
		//	return _htmlRegex.Replace(source, string.Empty);
		//}
		
		/// <summary>
		/// Remove HTML tags from string using char array.
		/// </summary>
		public static string StripTagsCharArray (this string source)
		{
			char[] array = new char[source.Length];
			int arrayIndex = 0;
			bool inside = false;
			
			for (int i = 0; i < source.Length; i++) {
				char let = source [i];
				if (let == '<') {
					inside = true;
					continue;
				}
				if (let == '>') {
					inside = false;
					continue;
				}
				if (!inside) {
					array [arrayIndex] = let;
					arrayIndex++;
				}
			}
			return new string (array, 0, arrayIndex);
		}

		public static string[] Split (this string s, string separator)
		{
			return s.Split (new string[] { separator }, System.StringSplitOptions.None);
		}

		public static int OccurenceCount (this string str, string val)
		{  
			int occurrences = 0;
			int startingIndex = 0;

			while ((startingIndex = str.IndexOf (val, startingIndex)) >= 0) {
				++occurrences;
				++startingIndex;
			}

			return occurrences;
		}

		public static int NthIndexOf (this string target, string value, int n)
		{
            
			string[] result = target.Split (value);
			n--;
			if (n >= 0 && n < result.Length) {
				int index = 0;
				for (int i = 0; i <= n; i++) {
					index += result [i].Length + value.Length; 
				}
				return index - value.Length;
			} else {
				return -1;
			}
		}

		public static bool Contains (this string source, string toCheck, StringComparison comp)
		{
			return source.IndexOf (toCheck, comp) >= 0;
		}
	}
}
