﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;

namespace ForieroEditor.Extensions
{
	public static partial class ForieroEditorExtensions
	{
		public static string FixOSPath (this string s)
		{
			#if UNITY_EDITOR_OSX || UNITY_EDITOR_LINUX
			return s.Replace (@"\", "/");
			#elif UNITY_EDITOR_WIN
			s = s.Replace("/", @"\");
			return s.Replace(@"\\", @"\"); 
			#else
			return s;
			#endif
		}

		public static string FixAssetsPath (this string s)
		{
			s = s.Replace (@"\", "/");
			return s.Replace ("//", "/");
		}

		public static string DoubleQuotes (this string s)
		{
			return "\"" + s + "\"";
		}

		public static string SingleQuotes (this string s)
		{
			return "'" + s + "'";
		}
	}
}
