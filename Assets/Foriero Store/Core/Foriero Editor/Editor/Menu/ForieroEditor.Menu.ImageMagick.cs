﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEditor;
using System.Text.RegularExpressions;
using System.IO;

using Image = UnityEngine.UI.Image;

namespace ForieroEditor
{
	public static partial class Menu
	{
		[MenuItem ("Assets/ImageMagick/Drop Shadow")]
		public static void ImageMagickDropShadow ()
		{
			switch (EditorUtility.DisplayDialogComplex ("Drop Shadow", "Drow Shadow selected Texture2D(s)?", "Yes - Suffix", "No", "Yes")) {
			case 0:
				Imagemagick.DropShadowSelected ("_shadow");
				break;
			case 1:

				break;
			case 2:
				Imagemagick.DropShadowSelected ("");
				break;
			}		
		}

		[MenuItem ("Assets/ImageMagick/Soft Edges")]
		public static void ImageMagickSoftEdges ()
		{
			switch (EditorUtility.DisplayDialogComplex ("Soft Edges", "Soft Edges selected Texture2D(s)?", "Yes - Suffix", "No", "Yes")) {
			case 0:
				Imagemagick.SoftEdgesSelected ("_softedges");
				break;
			case 1:

				break;
			case 2:
				Imagemagick.SoftEdgesSelected ("");
				break;
			}		
		}

		[MenuItem ("Assets/ImageMagick/Gray")]
		public static void ImageMagickGray ()
		{
			switch (EditorUtility.DisplayDialogComplex ("Gray", "Gray selected Texture2D(s)?", "Yes - Suffix", "No", "Yes")) {
			case 0:
				Imagemagick.GraySelected ("_gray");
				break;
			case 1:

				break;
			case 2:
				Imagemagick.GraySelected ("");
				break;
			}		
		}


		[MenuItem ("Assets/ImageMagick/Trim")]
		public static void ImageMagickTrim ()
		{
			switch (EditorUtility.DisplayDialogComplex ("Trim", "Trim selected Texture2D(s)?", "Yes - Suffix", "No", "Yes")) {
			case 0:
				Imagemagick.TrimSelected ("_trim");
				break;
			case 1:

				break;
			case 2:
				Imagemagick.TrimSelected ("");
				break;
			}		
		}

		[MenuItem ("Assets/ImageMagick/Watermark")]
		public static void ImageMagickWatermark ()
		{
			string watermark = Path.Combine (Directory.GetCurrentDirectory (), "watermark.png");
			if (File.Exists (watermark)) {
				switch (EditorUtility.DisplayDialogComplex ("Watermark", "Watermark selected Texture2D(s)?", "Yes - Suffix", "No", "Yes")) {
				case 0:
					Imagemagick.WatermarkSelected (watermark, "_watermark");
					break;
				case 1:

					break;
				case 2:
					Imagemagick.WatermarkSelected (watermark, "");
					break;
				}		
			} else {
				Debug.LogError ("File not exists : " + watermark);
			}
		}
	}
}
