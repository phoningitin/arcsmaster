﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

namespace ForieroEditor
{
	public static partial class Menu
	{
		[MenuItem ("Foriero/GitHub/Get Spine Runtime")]
		public static void GetSpineRuntime ()
		{
			string spine_unity = "https://github.com/EsotericSoftware/spine-runtimes/trunk/spine-unity/Assets/spine-unity/";
			string spine_src = "https://github.com/EsotericSoftware/spine-runtimes/trunk/spine-csharp/src/";


			GitHub.GetRepositoryFiles (spine_unity, "Assets/Spine/spine-unity");
			GitHub.GetRepositoryFiles (spine_src, "Assets/Spine/src");

			AssetDatabase.Refresh ();
		}

		//		[MenuItem ("Foriero/GitHub/Get Spine Runtime 3.x")]
		//		public static void GetSpineRuntime3x ()
		//		{
		//			string spine_unity = "https://github.com/EsotericSoftware/spine-runtimes/trunk/spine-unity/Assets/spine-unity/";
		//			string spine_src = "https://github.com/EsotericSoftware/spine-runtimes/trunk/spine-csharp/src/";
		//
		//			GitHub.GetRepositoryFiles (spine_unity, "Assets/Spine/spine-unity");
		//			GitHub.GetRepositoryFiles (spine_src, "Assets/Spine/src");
		//
		//			AssetDatabase.Refresh ();
		//		}

		[MenuItem ("Foriero/GitHub/Get DOTween")]
		public static void GetDOTween ()
		{
			string dotween = "https://github.com/Demigiant/dotween/trunk/_DOTween.Assembly/bin";
			GitHub.GetRepositoryFiles (dotween, "Assets/DOTween");
			AssetDatabase.Refresh ();
		}
	}
}