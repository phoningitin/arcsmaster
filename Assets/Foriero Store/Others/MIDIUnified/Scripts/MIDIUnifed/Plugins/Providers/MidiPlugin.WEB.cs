/* 
* 	(c) Copyright Marek Ledvina, Foriero Studo
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ForieroEngine.MIDIUnified.Plugins
{
	/*	
	 *  For web midi to work you need to install Jazz-Midi browser plugin http://www.jazz-soft.net/
	 * 
	 */
	
	

	#if UNITY_WEBPLAYER && !UNITY_EDITOR
	public struct MidiMessage
	{
		public byte Command;
		public byte Data1;
		public byte Data2;
		public int DataSize;
		public IntPtr DataPtr;
		
		public MidiMessage(MidiMessage aMidiMessage){
			Command = aMidiMessage.Command;
			Data1 = aMidiMessage.Data1;
			Data2 = aMidiMessage.Data2;
			DataSize = aMidiMessage.DataSize;
			DataPtr = aMidiMessage.DataPtr;
		}
		
		public byte[] GetData(){
			byte[] result = new byte[DataSize];
			Marshal.Copy(DataPtr, result, 0, DataSize);
			return result;
		}
		
		public byte[] GetBytes(){
			return new byte[3] {Command, Data1, Data2};
		}
		
		public void SetBytes(byte[] bytes){
			if(bytes.Length == 3){
				Command = bytes[0];
				Data1 = bytes[1];
				Data2 = bytes[2];
			}
		}
	}

	public static class MidiInPlugin {

		public static List<string> deviceNames = new List<string>();
		public static Queue<MidiMessage> midiMessages = new Queue<MidiMessage>(100);

		public static int ConnectDevice(int i){
			Application.ExternalCall("MidiIn_PortOpen", i);
			return 1;
		}
		
		public static int DisconnectDevice(){
			Application.ExternalCall("MidiIn_PortClose");
			return 1;
		}
				
		public static string GetDeviceName(int i){
			return deviceNames[i];
		}
		
		public static int GetDeviceCount(){
			return deviceNames.Count;
		}
		
		public static int PopMessage(out MidiMessage aMidiMessage){
		 	aMidiMessage = new MidiMessage();
			return 0;
		}
	}
	
	public static class MidiOutPlugin{

		public static List<string> deviceNames = new List<string>();

		public static int SendShortMessage(byte Command,byte Data1, byte Data2)
	    {
			Application.ExternalCall("SendMidiMessage", Command, Data1, Data2);
			return 0;
		}
				
		public static int ConnectDevice(int i){
			Application.ExternalCall("MidiOut_PortOpen", i);
			return 0;//MidiOut_PortOpen(i);
		}
		
		public static int DisconnectDevice(){
			Application.ExternalCall("MidiOut_PortClose");
			return 1;
		}
				
		
		public static string GetDeviceName(int i){
			return deviceNames[i];
		}
		
		public static int GetDeviceCount(){
	
			return deviceNames.Count;
		}
	}
	#endif
}
