﻿using UnityEngine;
using System.Collections;

public class SongSpawner : MonoBehaviour {

    private AudioSource myaudiosource;
    public float SongSpeed;
    public SongSequencer SequencerPrefab; //Contains all of the note data used to spawn
    private SongSequencer MySongSequencer;
    public TrailNote NotePrefab;
    public TrailMidpoint MidNotePrefab;
    public TrailNote[] ExistingNotes;
    
    public int NoteIndex;
	// Use this for initialization
	void Start () {
        NoteIndex = 0;
        myaudiosource = this.GetComponent<AudioSource>();
        SetupNoteSequence();


    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public enum Difficulty {Easy, Normal, Hard, Literal};
    public Difficulty SongDifficulty = Difficulty.Easy;
    public int NumberOfMidTrailBoxes = 9;
    public float XModifier = 1f;
    public float YModifier = 1f;
    public float PrimaryVelocity = 14f;
    public void SetupNoteSequence()
    {
        MySongSequencer = GameObject.Instantiate<SongSequencer>(SequencerPrefab);
        //GameMusic.clip = MySongSequencer.SongClip;
        SequencerNote[] sn = MySongSequencer.GetComponentsInChildren<SequencerNote>();

        //Sort them based on their x positions.
        bool sorted = false;
        while (!sorted)
        {
            sorted = true;
            for (int i = 1; i < sn.Length; i++)
            {

                SequencerNote n1, n2;
                n1 = sn[i - 1];
                n2 = sn[i];

                if (n1.transform.localPosition.x > n2.transform.localPosition.x)
                {
                    sn[i - 1] = n2;
                    sn[i] = n1;
                    sorted = false;
                    i--; //fewer iterations
                }
            }

        }



        //Spawn the Note Trails and provide ghosts
        Color notecolor = Color.white;
        switch (SongDifficulty)
        {
            case Difficulty.Easy: { notecolor = Color.blue; break; } //Few notes
            case Difficulty.Normal: { notecolor = Color.yellow; break; } //Some notes
            case Difficulty.Hard: { notecolor = Color.red; break; } //Many notes
            case Difficulty.Literal: { notecolor = Color.white; break; } //All notes


        }
        notecolor = Color.Lerp(notecolor,Color.white,.1f);

        TrailNote previousNote = null;
        int noteindex = 0;
        
        for (int i = 0; i < sn.Length; i++)
        {

            SequencerNote n = sn[i];
            if (!((n.EasyNote && (SongDifficulty == Difficulty.Easy)) ||
                    (n.NormalNote && (SongDifficulty == Difficulty.Normal)) ||
                    (n.HardNote && (SongDifficulty == Difficulty.Hard)) ||
                    ((SongDifficulty == Difficulty.Literal)))) continue;

            
            float distancebetweenbeats = (1f / (MySongSequencer.BPM / 60f));
            
            Vector3 pos = new Vector3(n.transform.localPosition.x* XModifier*(PrimaryVelocity*distancebetweenbeats),(n.transform.localPosition.y - MySongSequencer.CenterKeyPosition) *YModifier,0);
            TrailNote tn = GameObject.Instantiate(NotePrefab, this.transform.position+pos,new Quaternion()) as TrailNote;
            foreach (MeshRenderer mr in tn.GetComponentsInChildren<MeshRenderer>()) mr.material.color = notecolor;
            tn.GetComponent<LineRenderer>().material.color = notecolor;
            tn.GetComponent<Rigidbody>().velocity = new Vector3(-PrimaryVelocity, 0f, 0f);
            tn.MaxMidNotes = NumberOfMidTrailBoxes;
            tn.NoteIndex = noteindex++;

            if (previousNote != null)
            {

                //Spawn midpoint notes.
                int m = 0;
                for (int j = 1; j < (NumberOfMidTrailBoxes+1); j++)
                {
                    float f = (float)((float)j / ((float) (NumberOfMidTrailBoxes + 1)));
                    Vector3 posm = TrailNote.TrailLerp(previousNote.transform.position, tn.transform.position, f);
                    TrailMidpoint mp = GameObject.Instantiate(MidNotePrefab, posm, new Quaternion()) as TrailMidpoint;
                    mp.GetComponent<Rigidbody>().velocity = new Vector3(-PrimaryVelocity, 0f, 0f);
                    mp.NoteIndex = tn.NoteIndex;
                    mp.MidpointIndex = m;
                    m++;
                }
                tn.PreviousNote = previousNote;
                
            }
            //else
            //{
            //    Beginner b = (GameObject.Instantiate(BeginnerPrefab,tn.transform.position,new Quaternion()) as Beginner);
            //    b.transform.SetParent(tn.transform);
            //    b.transform.localPosition = new Vector3(0f,0f,0f);
            //    b.transform.position = b.transform.position + new Vector3(-(MySongSequencer.StartTimeOffset* PrimaryVelocity), 0f, 0f); 
            //    b.GetComponent<Rigidbody>().velocity = new Vector3(-PrimaryVelocity,0f, 0f);//tn.GetComponent<Rigidbody>().velocity;

            //    b.Music = GameMusic;

            //}
            previousNote = tn;


        }
    }
    public AudioSource GameMusic;
    public Beginner BeginnerPrefab;

    void OnDrawGizmosSelected()
    {
        //Gizmos.color = Color.red;
        //Gizmos.DrawLine(this.transform.position + new Vector3(1f,1f,0f), this.transform.position + new Vector3(1f, 1f, 0f) + new Vector3(XModifier,0f,0f));
        //Gizmos.color = Color.green;
        //Gizmos.DrawLine(this.transform.position + new Vector3(1f, 1f, 0f), this.transform.position + new Vector3(1f, 1f, 0f) + new Vector3(0f,YModifier, 0f));
        Gizmos.color = Color.cyan;

        float distancebetweenbeats = 1f;
        if (SequencerPrefab != null) distancebetweenbeats = (1f / (SequencerPrefab.BPM / 60f));
        else return;
        

        for (int i = 0; i < 8; i++)
            for (int j = -4; j <= 4; j++)
            {
                Gizmos.DrawSphere(this.transform.position + new Vector3(XModifier * (PrimaryVelocity * distancebetweenbeats) * i,YModifier*j,0f),.5f);

            }
    }
}
