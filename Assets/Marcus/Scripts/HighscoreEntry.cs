﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighscoreEntry : MonoBehaviour {


    public string Character;
    public int Score;
    public string Song;
    public string Difficulty;

    public bool IsHighlit;

    public Text ScoreText;
    public Text CharacterText;
    public Text SongText;
    public Text DifficultyText;

    public Image FreshEntryHighlight;
    
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        
        if (ScoreText) ScoreText.text = "" + Score;
        if (CharacterText) CharacterText.text = "" + Character;
        if (SongText) SongText.text = "" + Song;
        if (DifficultyText) DifficultyText.text = "" + Difficulty;
        FreshEntryHighlight.enabled = IsHighlit;
    }
}
