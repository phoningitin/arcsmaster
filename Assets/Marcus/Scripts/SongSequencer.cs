﻿using UnityEngine;
using System.Collections;

public class SongSequencer : MonoBehaviour {

    // Use this for initialization
    
    public float TestStartTime = 3f;
    public double StartPositionTime = 1.0;
    public bool TestSong = false;
    public bool musicPlaying = false;
    public string SongName = "Sample Song";
    public AudioClip SongClip;
    public float BPM = 150f;
    public float StartTimeOffset = 0f;
    public bool MoveCamera = true;
    public Camera MyCameraObject;
    private float CameraStartPosX = 0f;
    private float actualStartTime = 0f;
    public bool produceCurve = false;
    public float CenterKeyPosition = 0f;

    public LineRenderer mylinerenderer;

    private AudioSource myaudiosource;
    private SequencerNote[] sequencernotes;
	void Start () {
        myaudiosource = this.GetComponent<AudioSource>();
        mylinerenderer = this.GetComponent<LineRenderer>();
        sequencernotes = this.gameObject.GetComponentsInChildren<SequencerNote>();
        myaudiosource.clip = SongClip;
        actualStartTime = 0f;
        if (MyCameraObject)
        CameraStartPosX = MyCameraObject.transform.position.x;
        if (TestSong)
        {
            
            foreach (SequencerNote n in sequencernotes)
            {
                n.setGlowing(false);
            }
        } else
        {
            foreach (SequencerNote n in sequencernotes)
            {
                n.gameObject.SetActive(false);
            }
        }

        if (produceCurve) {
            //Use the Linerenderer
            mylinerenderer.enabled = true;
        } else
        {
            mylinerenderer.enabled = false;

        }

    }
	
	// Update is called once per frame
	void Update () {
        float elapsedtime = (Time.time - TestStartTime);

        if (TestSong && (elapsedtime >= 0))
        {
            if (!musicPlaying)
            {
                myaudiosource.Play();
                myaudiosource.timeSamples = (int)(44100.0 * StartPositionTime);
                musicPlaying = true;
               

                actualStartTime = Time.time;
                elapsedtime = (float)StartPositionTime;
            }
            else {
                elapsedtime = (myaudiosource.time-StartTimeOffset);
            }

            
            
            Debug.Log(elapsedtime);//(Time.time - actualStartTime);

            float onebeatlength = (60f / Mathf.Max(BPM, 0.001f));
        float beatspassed = (elapsedtime / onebeatlength); //current time

        foreach (SequencerNote n in sequencernotes)
            {
                float notelength = (n.getNoteLength() * onebeatlength);
               
                
                n.setGlowing((beatspassed >= n.getNoteStart()) && (beatspassed <= (n.getNoteStart() + n.getNoteLength())));

            }



            if (MyCameraObject) {
                MyCameraObject.transform.position = new Vector3(CameraStartPosX+(beatspassed+StartTimeOffset), MyCameraObject.transform.position.y, MyCameraObject.transform.position.z);
                }
        } 
	}

    public void createGhostCurve()
    {
        //This will generate a ghost effect along the notes using the linerenderer component.

    }

    void OnDrawGizmos()
    {
        //Maxes and Mins of the notes?
        Gizmos.color = Color.green;
        
        float x = (float)StartPositionTime;
        if (musicPlaying) x = myaudiosource.time;
        x = Mathf.Max((float)StartPositionTime,0f);
        //x *= 4f;
        float onebeatlength = (60f / Mathf.Max(BPM, 0.001f));
        float beatspassed = (x / onebeatlength); //current time
        x = beatspassed;
        Gizmos.DrawLine(new Vector3(x,20,0), new Vector3(x, -5, 0));
        float y = CenterKeyPosition;
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(new Vector3(0, y, 0), new Vector3(50, y, 0));
    }
}
