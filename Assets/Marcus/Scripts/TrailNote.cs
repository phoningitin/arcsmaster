﻿using UnityEngine;
using System.Collections;
using System;
using ForieroEngine.MIDIUnified.Plugins;
using ForieroEngine.MIDIUnified;
using System.Collections.Generic;
using System.Linq;

public class TrailNote : Scrollable {

    public TrailNote PreviousNote;
    public LineRenderer LineTrail;
    public int CurveSubdivisions = 4;
    public int NoteIndex;
    public float ForceValue = 0f;
    public float TrailWidth = 1f;
    public List<CreateNotes.CreateNoteNode> attachedNotes = new List<CreateNotes.CreateNoteNode>();
    public int MaxMidNotes;
    public bool PlayerCollided = false;
    public FeedbackText GoodText, NiceText, GreatText, PerfectText,PoorText;
    private FeedbackText created;
    public int NumberOfMidTrailBoxes = 9;
    public float PrimaryVelocity = 14f;
    public ParticleSystem explosionParticles,PerfectGlowParticles,ChainFireworksParticles,MissDustParticles;
    public List<GameObject> toDeactivateOnNotePlayed;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
        FadeAwayValue = 0f;
        PlayerCollided = false;
    }


    public float FadeAwayValue = 0f;
	
    public static Vector3 TrailLerp(Vector3 prev,Vector3 next,float f)
    {

        //float dif = (next.x - prev.x);
        //float factor = 1f;// (.1f* Mathf.Abs(next.y - prev.y));
        //return Vector3.Lerp(prev, next, f) + (Mathf.Sin((f)*Mathf.PI*2f)*(new Vector3(dif*factor,0f,0f)));
        Vector3 dif = (next - prev);
        return (prev + new Vector3(dif.x*f,0f,0f) + new Vector3(0f,(dif.y/2f)- ((dif.y / 2f) * Mathf.Cos(f * Mathf.PI)), 0f));
    }
    // Update is called once per frame
    public Color NoteColor = Color.white;
    void Update () {
	
        if ((PreviousNote != null) && (this.gameObject.activeSelf) && (PreviousNote.isActiveAndEnabled) && (PreviousNote.transform.position.x < transform.position.x))
        {
            //Draw the line in a Curve depending on the angle and strength.
            int v = (CurveSubdivisions * 2);
            LineTrail.SetVertexCount(v+1);
            LineTrail.SetWidth(PreviousNote.TrailWidth, TrailWidth);
            LineTrail.SetColors(Color.Lerp(Color.white,Color.clear,PreviousNote.FadeAwayValue), Color.Lerp(Color.white, Color.clear, FadeAwayValue));
            for (int i = 0; i <= v; i++)
            {
                float f = (((float)i)/(v+1));
                if (i == v)
                {
                    LineTrail.SetPosition(i, this.transform.position);

                } else {
                    
                    Vector3 p = TrailLerp(PreviousNote.transform.position, this.transform.position, f);
                    LineTrail.SetPosition(i, p);

                }
                

            }
            
            LineTrail.useWorldSpace = true;
            LineTrail.enabled = true;
        } else
        {

            LineTrail.enabled = false;
        }
	}

    void LateUpdate()
    {
        
        if (FadeAwayValue >= 1f)
        {
            this.gameObject.SetActive(false);
        } else if (FadeAwayValue > 0f)
        {
            foreach(MeshRenderer mr in this.gameObject.GetComponentsInChildren<MeshRenderer>())
            {
                mr.material.color = Color.clear;//Color.Lerp(mr.material.color,Color.clear,FadeAwayValue * .9f);
                Color col = this.GetComponentInChildren<LineRenderer>().material.GetColor("_TintColor");
                this.GetComponentInChildren<LineRenderer>().material.SetColor("_TintColor", Color.Lerp(col, Color.clear, FadeAwayValue * .9f));
            }
                
        }
    }
    public static int TrailNoteChainBoostThreshold = 5;
    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player") || col.CompareTag("Receiver"))
            GameState.StartSong();
        if ((col.CompareTag("Player")) && (!PlayerCollided))
        {

            PlayerCollided = true;
            PlayerInput plr = col.gameObject.GetComponent<PlayerInput>();
            FeedbackText ft = GoodText;
            float followedpathvalue = 0f;
            int scorevalue = 100; //Multiply by game speed.
            int followlevel = 0;
            float delta = 0f;
            if ((PreviousNote != null) && (PreviousNote.transform.position.x < this.transform.position.x))
            {
                delta = Mathf.Abs((this.transform.position - PreviousNote.transform.position).normalized.y);
            }
            if (plr.MidNoteCollidedIndex == NoteIndex)
            {
                followedpathvalue = (float)(((float)plr.MidNotesCollided) / ((float)MaxMidNotes));
                plr.TrailNoteChain += 1;
            }
            else {
                followedpathvalue = 0f;
                plr.TrailNoteChain = 0;
            }

            float misstimeoffset = Mathf.Abs(this.GetComponent<Collider>().transform.position.y - col.transform.position.y);
            plr.TrailNoteCollidedIndex = NoteIndex;
            GameState.GetInst().LastTrailNoteHitTime = Time.time;
            //Debug.Log("Miss: " + misstimeoffset);
            bool chained = ((PreviousNote == null) || (PreviousNote.PlayerCollided));
            if ((misstimeoffset > .5f)) {

                ft = PoorText; followlevel = 0;
            } else {
                if (!chained) { ft = GoodText; followlevel = 1; } else {
                    if (followedpathvalue >= 1f) { ft = PerfectText; followlevel = 4; }
                    else if (followedpathvalue >= .6f) { ft = GreatText; followlevel = 3; }
                    else if (followedpathvalue >= .3f) { ft = NiceText; followlevel = 2; }
                    else { ft = GoodText; followlevel = 1; }
                }
            }

            if ((delta >= .56) && (PreviousNote.PlayerCollided) && (followlevel < 3))
            {
                //Its hard chaining these together man. here's some extra credit for doing so.
                ft = GreatText; followlevel = 3;
            }
            //created = GameObject.Instantiate<FeedbackText>(ft) as FeedbackText;

            //created.transform.SetParent(this.transform);
            //created.transform.localPosition = new Vector3(0f, 0f, 0f);
            //created.transform.position = created.transform.position + new Vector3(0f, 0f, -5f);

            int sc = scorevalue;
            FeedbackHolder.showFeedback(followlevel, this.transform.position);
            float increaseSlowMo = 0f;
            switch (followlevel)
            {
                case 0: { scorevalue = 15; increaseSlowMo = 0f; break; }
                case 1: { scorevalue = 20; increaseSlowMo = .005f; break; }
                case 2: { scorevalue = 40; increaseSlowMo = .02f; break; }
                case 3: { scorevalue = 60; increaseSlowMo = .05f; break; }
                case 4: { scorevalue = 100; increaseSlowMo = .08f; break; }
            }
            scorevalue = Mathf.FloorToInt(((float)scorevalue) * GameState.GameSpeed);
            if (GameState.GetInst().CowbellBonus) {
            scorevalue *= 2;
                float pi = 1f + ((this.transform.position.y - GameState.GetInst().CowbellBasePitchY) / 8f);
                AudioCarrier.getInst().playCowbellSound(pi);
            }

            if (GameState.CurrentState == GameState.State.InProgress)
            {
                GameState.addSlowMo(increaseSlowMo);
            }
            if (followlevel > 2)
                if (plr.TrailNoteChain >= TrailNoteChainBoostThreshold) {
                GameState.SpeedBoost();
            }
            ScoreKeeper.AddScore(scorevalue);


            foreach (CreateNotes.CreateNoteNode node in attachedNotes)
            {
                if (node.timeMarker == 0f)
                    node.timeMarker = Time.timeSinceLevelLoad;
                StartCoroutine(playNote(node));
            }

            if (explosionParticles)
            {
                explosionParticles.startColor = this.GetComponentInChildren<MeshRenderer>().material.color;

                if (PerfectGlowParticles && (followlevel >= 4))
                {
                    PerfectGlowParticles.startColor = this.GetComponentInChildren<MeshRenderer>().material.color;
                    PerfectGlowParticles.Play();
                }
                if (ChainFireworksParticles)
                {
                    ChainFireworksParticles.startColor = this.GetComponentInChildren<MeshRenderer>().material.color;
                    int n = (ScoreKeeper.getInst().getCombo());

                    ChainFireworksParticles.Emit(n);
                }
                
                explosionParticles.Play();
            }
            foreach (GameObject obj in toDeactivateOnNotePlayed)
                obj.SetActive(false);
        }
        else if (col.GetComponent<BreakCombo>() && (!PlayerCollided))
        {
            foreach (CreateNotes.CreateNoteNode node in attachedNotes.Where(note => !note.on))
            {
                StartCoroutine(playNote(node));
            }
            
            GameState.SpeedMiss();
        }
        else if (col.CompareTag("TimeMarker") && (!PlayerCollided))
        {
            foreach (CreateNotes.CreateNoteNode note in attachedNotes)
                note.timeMarker = Time.timeSinceLevelLoad;
        }
    }

    IEnumerator playNote(CreateNotes.CreateNoteNode note)
    {
        float waitTime = (GameState.NoteInterval - note.timeDifferential + (note.timeMarker - Time.timeSinceLevelLoad));
        if (waitTime > 0)
            yield return new WaitForSeconds(waitTime);
        if(note.on)
            MidiOut.NoteOn(Mathf.Clamp(note.MotionCorrectedIndex,0,127), Mathf.Clamp(note.aVolume, 0, 127), note.channel);
        else
            MidiOut.NoteOff(Mathf.Clamp(note.MotionCorrectedIndex, 0, 127), note.channel);
    }

    internal void AttachNotes(List<CreateNotes.CreateNoteNode> nodeCreation)
    {
        attachedNotes.Clear();
        attachedNotes.AddRange(nodeCreation);
    }

    public bool midpointsadded = false;
    public TrailMidpoint MidNotePrefab;
    
    internal void addMidpoints()
    {
        if (midpointsadded) return;
        if (PreviousNote == null) return;
                midpointsadded = true;
        int m = 0;
        float spe = GameState.GameSpeed;
        int mc = 0;
        TrailMidpoint prevpoint = null;
        for (int j = 1; j < (NumberOfMidTrailBoxes + 1); j++)
        {
            float f = (float)((float)j / ((float)(NumberOfMidTrailBoxes + 1)));
            Vector3 posm = TrailNote.TrailLerp(PreviousNote.transform.position, transform.position, f);
            TrailMidpoint mp =TrailMidpoint.SpawnMidnote(posm);
            if (prevpoint != null) prevpoint.nextpoint = mp.transform;
            
            if (mp != null)
            {
                mp.noteStart();
                mp.transform.parent = this.transform;
                //mp.GetComponent<Rigidbody>().velocity = new Vector3(-PrimaryVelocity * spe, 0f, 0f);
                mp.NoteIndex = NoteIndex;
                mp.MidpointIndex = m;
                mc++;

            } else
            {
                Debug.Log("Missing Midpoint");

            }
            m++;
            prevpoint = mp;
        }
        if (prevpoint != null) prevpoint.nextpoint = this.transform;
        this.MaxMidNotes = mc;
    }

    protected override void DoReset()
    {
        base.DoReset();
        PlayerCollided = false;
        if(created)
            created.gameObject.SetActive(false);
        if (explosionParticles)
            explosionParticles.Stop(true);
        foreach (GameObject obj in toDeactivateOnNotePlayed)
            obj.SetActive(true);
    }

    void OnDisable()
    {
        transform.localScale = baseScale;
        PreviousNote = null;
        foreach (GameObject obj in toDeactivateOnNotePlayed)
            obj.SetActive(true);
    }
}
