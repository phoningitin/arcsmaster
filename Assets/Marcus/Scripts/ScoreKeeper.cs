﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour {

    // Use this for initialization
    public static int Score { get { if (_inst == null) return 0; else return _inst._playerScore; } }
    private int _playerScore = 0;
    public static int Combo { get { if (_inst == null) return 0; else return _inst._combo; } }
    private int _combo;
    public int NotesHit = 0;
    public int NotesMissed = 0;
    public Text ScoreText;
    public Image ScoreImage;
    private static ScoreKeeper _inst = null;
    public static GameState.SimpleAction OnScore;
    public static GameState.SimpleAction OnMiss;

    void Start () {
        _playerScore = 0;
        _inst = this;
        OnScore += () => { };
        OnMiss += () => { };
    }

    void OnDestroy()
    {
        _inst = null;
        OnScore -= OnScore;
        OnMiss -= OnMiss;
    }

    public static ScoreKeeper getInst()
    {
        return _inst;
    }
    public int getCombo()
    {
        return _combo;
    }
    public static void UpdateScoreText()
    {
        if (_inst == null) return;
        string s = ("" + _inst._playerScore);
        string zeroes = "";
        for (int i = s.Length; i < 5; i++)
            zeroes += "0";
        _inst.ScoreText.text = (zeroes+s);
    }

    public static float CompletionPercentage { get { if (_inst == null) return 0f; else return ((float)_inst.NotesHit / (float)(_inst.NotesHit + _inst.NotesMissed)); } }

    public static void MissedNote()
    {
        if(!(GameState.CurrentState == GameState.State.InProgress || GameState.CurrentState == GameState.State.SlowMo))
        if (_inst == null) return;
        GameState.GetInst().CowbellBonus = false;
        AudioCarrier.getInst().playMissChord(_inst._combo);
        _inst._combo = 0;
        _inst.NotesMissed++;
        OnMiss();
        SpawnObstacles.spawnTomato();
    }
    
    public static void AddScore(int Score)
    {
        if (_inst == null) return;
        _inst._playerScore += Score;
        _inst._combo++;
        _inst.NotesHit++;
        UpdateScoreText();
        OnScore();
    }
}
