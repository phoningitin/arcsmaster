﻿using UnityEngine;
using System.Collections;

public class TrailMidpoint : MonoBehaviour {

    // Use this for initialization

    public int NoteIndex=0,MidpointIndex=0;
	
    public bool PlayerCollided = false;
    public static int MaxMidnotes = 300;
    private static TrailMidpoint[] ExistingMidnotes;
    private static bool midpointscreated = false;
    private static TrailMidpoint _inst;
    void Start()
    {
        _inst = this;
    }

    void OnDestroy()
    {
        ExistingMidnotes = null;
        _inst = null;
        midpointscreated = false;
    }

    public void noteStart()
    {
        PlayerCollided = false;

    }
    public Transform nextpoint;
    void OnTriggerEnter(Collider col)
    {
        if ((col.CompareTag("Player")) && (!PlayerCollided))
        {
            PlayerCollided = true;
            PlayerInput plr = col.gameObject.GetComponent<PlayerInput>();
            if (plr.TrailNoteMidCollidedIndex != NoteIndex)
            {

                plr.TrailNoteMidCollidedIndex = NoteIndex;
                plr.TrailNoteCollidedIndex = NoteIndex;
                plr.MidNoteCollidedIndex = MidpointIndex;
                Debug.Log("Midpoint: " + plr.MidNoteCollidedIndex);
                plr.MidNotesCollided = 1;
                
            } else
            {
                
                plr.MidNoteCollidedIndex = NoteIndex;
                plr.MidNotesCollided += 1;
                Debug.Log("Mid Chain: " + plr.MidNotesCollided);
            }

            
           


        }


    }

    void OnTriggerStay(Collider col)
    {
        PlayerInput plr = col.gameObject.GetComponent<PlayerInput>();
        if ((col.CompareTag("Player")) && (plr != null) && (plr.MidNoteCollidedIndex == MidpointIndex))
        {

            
            if (GameState.GetInst().AutoVelocity)
            {
                Debug.Log("Auto");
                Transform t = nextpoint;
                if ((t != null) && (true||(plr.transform.position.x <= t.position.x)))
                {
                    Vector3 tpos = plr.transform.position;
                    Vector3 dif = (t.transform.position - tpos);

                    Vector3 pos = new Vector3(plr.transform.position.x, tpos.y, plr.transform.position.z);
                    plr.transform.position = Vector3.Lerp(plr.transform.position,pos,1f);
                    plr.transform.GetComponent<Rigidbody>().velocity = new Vector3();
                }
            }

        }
    }

    private static int ExistingMidnoteIndex = 0;
    public TrailMidpoint MidnotePrefab;
    public static void setupMidnotes()
    {
        ExistingMidnoteIndex = 0;
        midpointscreated = true;
        ExistingMidnotes = new TrailMidpoint[MaxMidnotes];
        if (_inst == null)
        {
            _inst = GameObject.FindObjectOfType<CreateNotes>().prefab.MidnotePrefab;
        }
        for (int i = 0; i < MaxMidnotes; i++)
        {
            ExistingMidnotes[i] = GameObject.Instantiate<TrailMidpoint>(_inst.MidnotePrefab);
            ExistingMidnotes[i].gameObject.SetActive(false);

        }

    }
    private static TrailMidpoint nextAvailableMidpoint()
    {
        if (!midpointscreated) setupMidnotes();
        for (int i = 0; i < MaxMidnotes; i++)
        {
            TrailMidpoint mp = ExistingMidnotes[(ExistingMidnoteIndex + i) % MaxMidnotes];
            if (!mp.gameObject.activeSelf)
            {
                //use this one, its not in use.
                ExistingMidnoteIndex = ((ExistingMidnoteIndex + i + 1) % MaxMidnotes);
                return mp;
            }
        }
        Debug.Log("Too many midnotes!");
        return null;
    }
    public static TrailMidpoint SpawnMidnote(Vector3 pos)
    {
        if (!midpointscreated)
        {
            setupMidnotes();
        }
        TrailMidpoint mp = nextAvailableMidpoint();

        if (mp != null)
        {
            mp.transform.position = pos;
            mp.gameObject.SetActive(true);

        }


        return mp;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawSphere(this.transform.position, .2f);
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(this.transform.position,.4f);
    }
}
