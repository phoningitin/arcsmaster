﻿using UnityEngine;
using System.Collections;

public class FlameBar : MonoBehaviour {

    // Use this for initialization
    private float PreferredSize;
    private float PreferredParticleCount;
    public ParticleSystem FlameEffect;
    public float FlameValue = 0f;
    public Vector3 startPosition;
    public Color MinColor, MaxColor,BurningColor;
    public float Incinerating = 0f;
    private bool starting = false;
    void Start () {
        PreferredSize = FlameEffect.shape.box.x;
        PreferredParticleCount = FlameEffect.emission.rate.constantMax;
        starting = true;
        startPosition = FlameEffect.transform.position - new Vector3(PreferredSize / 2f, 0f, 0f);
    }
	
	// Update is called once per frame
	void Update () {

        float power = FlameValue; //0.0-1.0
        float barsize = Mathf.Clamp01(FlameValue);
        float lerpv = (1f / 10f);
        if (starting)
        {
            starting = false;
            lerpv = 1f;
        }
        FlameEffect.transform.localScale = Vector3.Lerp(FlameEffect.transform.localScale,new Vector3(barsize, 1f, 1f),lerpv);
        FlameEffect.transform.position = Vector3.Lerp(FlameEffect.transform.position,(startPosition + (new Vector3(PreferredSize/2f,0f,0f)* barsize)),lerpv);
        
        //change the color

        //Rising flames so fast
        
        float raisebase = -.13f;
        float raisemax = -.8f;
        float inc = Incinerating;
        
            FlameEffect.gravityModifier = Mathf.Lerp(FlameEffect.gravityModifier,Mathf.Lerp(raisebase, raisemax, Incinerating), lerpv);
            FlameEffect.startColor = Color.Lerp(FlameEffect.startColor,Color.Lerp(MaxColor, BurningColor, Incinerating), lerpv);
        

        
        

    }
}
