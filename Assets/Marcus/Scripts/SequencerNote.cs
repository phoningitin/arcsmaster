﻿using UnityEngine;
using System.Collections;

public class SequencerNote : MonoBehaviour
{

    //The properties of a sequencer note can be determined by its transform's local space
    //Time is counted in beats or .25 measures, not actual seconds. i.e. a position
    public bool IsNote = false;
    
    public bool EasyNote = false, NormalNote = false, HardNote = false;
    private bool IsGlowing = false;
    public MeshRenderer MyRenderer;
    void Start()
    {
        setGlowing(IsGlowing);

    }
    public float getNoteStart()
    {
        return transform.localPosition.x;
    }

    //Pitch is measured in one step per Y unit
    public float getNotePitch()
    {

        return transform.localPosition.y;
    }

    //Length is measured in beats
    public float getNoteLength()
    {

        return transform.localScale.x;
    }

    public void setGlowing(bool gl)
    {
        if (IsGlowing == gl) return;
        Debug.Log(gl);
        IsGlowing = gl;
        MyRenderer.material.SetColor("_Color", (gl ? Color.white : Color.black));
    }

    void OnDrawGizmos()
    {
        if (IsNote)
        {

            if (EasyNote) {
                Gizmos.color = Color.blue;
                Gizmos.DrawWireCube(this.transform.position + new Vector3(.5f, .5f, 0f), new Vector3(.8f, .8f, 1f));
            }

            if (NormalNote)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireCube(this.transform.position + new Vector3(.5f, .5f, 0f), new Vector3(.5f, .5f, 1f));
            }
            if (HardNote)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireCube(this.transform.position + new Vector3(.5f, .5f, 0f), new Vector3(.2f, .2f, 1f));
            }

        }

    }
}