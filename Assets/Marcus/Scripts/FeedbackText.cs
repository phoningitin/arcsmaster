﻿using UnityEngine;
using System.Collections;
using System;

public class FeedbackText : MonoBehaviour {

    public AnimationCurve AnimationCurveValue;
    public float Duration = 2f;
    private float StartTime;
    private Vector3 PrimaryScale;
	// Use this for initialization
	void Start () {
        PrimaryScale = transform.localScale;
        StartTime = Time.time;
    }
	
	// Update is called once per frame
	void Update () {

        float f = ((Time.time - StartTime)/Duration);
        if (f < 1f)
        {
            this.transform.localScale = (PrimaryScale * AnimationCurveValue.Evaluate(f));


        } else
        {
            if (this.gameObject.activeSelf)
            this.gameObject.SetActive(false);
        }
	}

    public void Restart()
    {
        StartTime = Time.time;
        if (!this.gameObject.activeSelf)
            this.gameObject.SetActive(true);

    }
}
