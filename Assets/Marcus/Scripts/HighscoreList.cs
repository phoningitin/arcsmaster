﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighscoreList : MonoBehaviour {

    // Use this for initialization
    public HighscoreEntry[] Entries;
    public HighscoreEntry EntryPrefab;
    public int FreshEntry = -1;
    public static int maxEntries = 8;
	void Start () {
        loadHighscores();

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void loadHighscores()
    {
        

        VictoryStats.prepareHighScores();
        Vector3 startpos = this.transform.position;
        int entrycount = VictoryStats.MaxHighScores;
        //Entries = new HighscoreEntry[entrycount];
        for (int i = 0; i < entrycount; i++)
        {
            HighscoreEntry ent;
            //if (i != 0)
                ent = Entries[i];//(GameObject.Instantiate(EntryPrefab, transform.position, new Quaternion()) as HighscoreEntry); else ent = EntryPrefab;
            //ent.transform.parent = this.transform.parent;
            //ent.transform.Translate(new Vector3(0f, -1f*i, 0f));
            ent.IsHighlit = false;
            ent.Character = PlayerPrefs.GetString("HighCharacter" + i);
            ent.Difficulty = PlayerPrefs.GetString("HighDifficulty" + i);
            ent.Song = PlayerPrefs.GetString("HighSong" + i);
            ent.Score = PlayerPrefs.GetInt("HighScore" + i);
            Entries[i] = ent;
        }
        //PlayerPrefs.SetInt("highscoreindex", highscoreindex);





    }
}
