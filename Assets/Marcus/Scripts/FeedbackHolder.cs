﻿using UnityEngine;
using System.Collections;

public class FeedbackHolder : MonoBehaviour {

    // Use this for initialization
    public FeedbackText PoorText, GoodText, NiceText, GreatText, PerfectText;
	void Start () {
        if (_inst == null) _inst = this;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.Translate(new Vector3(0f,1f*Time.deltaTime,0f));
	
	}
    public static FeedbackHolder _inst=null;
    public static FeedbackHolder getInst()
    {
        return _inst;
    }
    
    public static void showFeedback(int fb,Vector3 pos)
    {

        if (((_inst == null) || (!_inst.gameObject.activeSelf))) return;

        FeedbackText[] fblist = { _inst.PoorText, _inst.GoodText, _inst.NiceText, _inst.GreatText, _inst.PerfectText };
        FeedbackText te = fblist[fb];
        foreach (FeedbackText f in fblist)
        {

            if (f.gameObject.Equals(te.gameObject))
            {
                //f.gameObject.SetActive(true);
                f.Restart();
            } else
            {
                if (f.gameObject.activeSelf)f.gameObject.SetActive(false);
            }

        }
        _inst.transform.position = pos+new Vector3(-2f,2f,-5f);
        
        

    }
}
