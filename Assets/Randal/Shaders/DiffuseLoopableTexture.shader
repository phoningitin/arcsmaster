﻿// Unlit alpha-cutout shader.
// - no lighting
// - no lightmap support
// - no per-material color

Shader "Custom/DiffuseLoopableTexture" {
	Properties{
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
	_TexMoveSpeedX("Texture Move Speed X", float) = 1
		_TexMoveSpeedY("Texture Move Speed Y", float) = 0
		_TexTransformOffset("Texture Offset", Vector) = (1,1,0,0)
		_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
	}
		SubShader{
		Tags{ "Queue" = "AlphaTest" "IgnoreProjector" = "True" "RenderType" = "TransparentCutout" }
		LOD 100

		Lighting Off

		Pass{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

	struct appdata_t {
		float4 vertex : POSITION;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f {
		float4 vertex : SV_POSITION;
		half2 texcoord : TEXCOORD0;
	};

	sampler2D _MainTex;
	float _TexMoveSpeedX;
	float _TexMoveSpeedY;
	float4 _TexTransformOffset;
	float4 _MainTex_ST;
	fixed _Cutoff;

	v2f vert(appdata_t v)
	{
		v2f o;
		o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
		//o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

		_TexTransformOffset.z += _Time.y * 0.05 * _TexMoveSpeedX;
		o.texcoord = v.texcoord.xy * _MainTex_ST.xy + _TexTransformOffset.zw;
		return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{
		fixed4 col = tex2D(_MainTex, i.texcoord);
	clip(col.a - _Cutoff);
	return col;
	}
		ENDCG
	}
	}

}
