﻿using UnityEngine;
using System.Collections;

public class BreakCombo : MonoBehaviour
{
    void OnTriggerEnter(Collider c)
    {
        if (c.CompareTag("Note"))
            if (!c.GetComponent<TrailNote>().PlayerCollided)
            {
                ScoreKeeper.MissedNote();
                Animator anim = c.GetComponent<Animator>();
                
                anim.SetInteger("MissRandom",Random.Range(0,2));
                anim.SetTrigger("OnMiss");
                TrailNote t = c.gameObject.GetComponent<TrailNote>();
                if (t != null)t.MissDustParticles.Play();

            }

    }
}
