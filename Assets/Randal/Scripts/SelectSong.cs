﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class SelectSong : MonoBehaviour
{
    Dropdown drop;
    public string SongName;

    [System.Serializable] public class PlayableMidi
    {
        public string name;
        public TextAsset midiFile;
        public AudioClip analogSoundFile;
    }
    [SerializeField] List<PlayableMidi> midiList;
    public PlayableMidi selectedMidi { get { return midiList.Find(midi => midi.name == SongName); } }

    void Awake()
    {
        drop = GetComponent<Dropdown>();
        SongName = PlayerPrefs.GetString("SongName");
        if(drop)
            drop.value = drop.options.FindIndex(op => op.text == SongName);
        if (GetComponent<MidiSeqKaraokeScript>())
            GetComponent<MidiSeqKaraokeScript>().midiFileTextAsset = selectedMidi.midiFile;
    }

    public void OnValueChanged(int choice)
    {
        SongName = drop.options[choice].text;
        PlayerPrefs.SetString("SongName", SongName);
        PlayerPrefs.Save();
    }
}
