﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PII;

public class MIDI_Player : MonoBehaviour
{
    [System.Serializable] public class AudioSample
    {
        public AudioClip clip;
        public string name;
        public int midiCode;
    }
    public string songName;
    private MidiEventSequence sequence;
    public AudioHandler audioOut;
    public List<AudioSample> samples;

    void Start ()
    {
        sequence = MIDIsequence.getEventSequence(songName);
        if (sequence != null)
            playSequence();
        else
            Debug.Log("null sequence...");
	}

    void playSequence()
    {
        Debug.Log("events in sequence: " + sequence.events.Count());
        foreach (MidiEventSequence.PIIEvent evt in sequence.events)
            if(evt.code == MidiCommandCode.NoteOn)
                StartCoroutine(midiEvent(evt));
    }

    IEnumerator midiEvent(MidiEventSequence.PIIEvent evt)
    {
        yield return new WaitForSeconds(evt.timeMarker);
        AudioSample sample = samples.Find(s => s.name == evt.NoteName || s.midiCode == evt.noteNumber);
        int freqDiff = 0;
        //if no matching sample (in terms of note name or midi code (0-127)), then sort existing samples and find
        //the closest match. Then, pitch shift that closest matching sample (in AudioHandler) to try to get the right note.
        if (sample == null)
        {
            List<AudioSample> closest = new List<AudioSample>();
            closest.AddRange(samples);
            closest.Sort((s1, s2) => Mathf.Abs(evt.noteNumber - s1.midiCode).CompareTo(Mathf.Abs(evt.noteNumber - s2.midiCode)));
            sample = closest.First();
            freqDiff = evt.noteNumber - sample.midiCode;
        }

        audioOut.PlayEffect(sample.clip, ((float)evt.velocity)/(float)(127), freqDiff);
    }
}
