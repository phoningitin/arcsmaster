﻿using UnityEngine;
using System.Xml.Serialization;
using System.IO;

using Newtonsoft.Json;


public static class SerializeXML
{
	public static T DeserializeTextAssetFromResources<T>(string filename)
	{
		TextAsset textAsset = (TextAsset)Resources.Load(filename, typeof(TextAsset));
		if(textAsset == null)
			return default(T);
		return DeserializeObject<T>(textAsset);
	}
	
	public static void SerializeObjectToTextAsset<T>(string filename, T data)
	{
		XmlSerializer serializer = new XmlSerializer(typeof(T));
		TextWriter textWriter = new StreamWriter(filename);
		serializer.Serialize(textWriter, data);
		textWriter.Close();
	}

    public static T DeserializeObject<T>(string filepath)
    {
        byte[] serializedData = File.ReadAllBytes(filepath);
        T returnObject;
        
        using (var stream = new StreamReader(new MemoryStream(serializedData)))
        {
            using (JsonReader reader = new JsonTextReader(stream))
            {
                JsonSerializer serializer = new JsonSerializer();
                returnObject = (T)serializer.Deserialize<T>(reader);
            }
        }

        return returnObject;
    }

    public static T DeserializeObject<T>(TextAsset asset)
    {
        T returnObject;

        //Now that we have a byte array of our serialized data, let's Deserialize it.
        using (var stream = new StreamReader(new MemoryStream(asset.bytes)))
        {
            using (JsonReader reader = new JsonTextReader(stream))
            {
                JsonSerializer serializer = new JsonSerializer();
                returnObject = (T)serializer.Deserialize<T>(reader);
            }
        }

        //	//Debug.Log(returnObject);
        return returnObject;
    }

    public static void SerializeObject<T>(string path, T data)
	{
		byte[] serializedData = new byte[]{};

        //Create a memory stream to hold the serialized bytes
        using (var mem = new MemoryStream())
        {
            using (var stream = new StreamWriter(mem))
            {
                using (JsonWriter writer = new JsonTextWriter(stream))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(writer, data);
                }
                serializedData = mem.ToArray();
                File.WriteAllBytes(path, serializedData);
            }
        }
        

		//Debug.Log(serializedData);
	}
}
