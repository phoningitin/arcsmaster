﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowCombo : MonoBehaviour
{
    private Text comboText;
    public Text feedback1;
    public Text feedback2;
    public Text feedback3;
    public ParticleSystem comboParticles;
    private AudioSource myAudioSource;
    public AudioClip[] ComboChords;
    void Awake()
    {
        comboText = GetComponent<Text>();
        ScoreKeeper.OnScore += UpdateCombo;
        ScoreKeeper.OnMiss += UpdateCombo;
        UpdateCombo();
        feedback1.text = "";
        feedback2.text = "";
        feedback3.text = "";
        comboParticles.Clear();
        feedback3.GetComponent<RainbowColor>().enabled = false;
        myAudioSource = this.GetComponent<AudioSource>();
    }

    void UpdateCombo()
    {
        if (ScoreKeeper.Combo == 0)
        {
            comboText.text = "";
        }
        else
        {
            float sizenumber = Mathf.Lerp(20,60,(((float)ScoreKeeper.Combo)/100f));
            comboText.text = "<size="+((int)sizenumber)+">" + ScoreKeeper.Combo.ToString() + "xChain!</size>";
            if(ScoreKeeper.Combo == 20)
            {
                AudioCarrier.getInst().playChordSubtle();
                feedback1.text = "20 CHAIN!";
                feedback2.text = "Clyde must stand for";
                feedback3.text = "<size=60>CLEAN COMBO!</size>";
                comboParticles.Play();
                StartCoroutine(hideFeedback());
            }
            if (ScoreKeeper.Combo == 50)
            {
                AudioCarrier.getInst().playChordMajor(1);
                feedback1.text = "30 CHAIN!";
                feedback2.text = "Clyde must stand for";
                feedback3.text = "<size=70>CRISP COMBO!!</size>";
                comboParticles.Play();
                StartCoroutine(hideFeedback());
            }
            if(ScoreKeeper.Combo == 80)
            {
                AudioCarrier.getInst().playChordMajor(2);
                feedback1.text = "50 CHAIN!";
                feedback2.text = "Clyde must stand for";
                feedback3.text = "<size=80>COOL COMBO!!</size>";
                comboParticles.Play();
                StartCoroutine(hideFeedback());
            }
            if (ScoreKeeper.Combo == 100)
            {
                AudioCarrier.getInst().playChordMajor(3);
                feedback1.text = "100 CHAIN!";
                feedback2.text = "Clyde must stand for";
                feedback3.text = "<size=90>COLOSSAL COMBO!!!!</size>";
                comboParticles.Play();
                StartCoroutine(hideFeedback());
            }
        }
    }
    IEnumerator hideFeedback()
    {
        feedback3.GetComponent<RainbowColor>().enabled = true;
        yield return new WaitForSeconds(2f);
        feedback1.text = "";
        feedback2.text = "";
        feedback3.text = "";
        comboParticles.Stop();
        feedback3.GetComponent<RainbowColor>().enabled = false;
    }
}
