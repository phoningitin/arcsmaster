﻿using UnityEngine;
using System.Collections;

public class Scrollable : MonoBehaviour
{
    public Vector3 RotationalVelocityMin;
    public Vector3 RotationalVelocityMax;
    public Vector3 VelocityMin;
    public Vector3 VelocityMax;
    protected Vector3 velocity;
    protected Vector3 rotationalVelocity;
    protected Vector3 initialVelocity;
    protected Rigidbody rb;
    protected Rigidbody2D rb2d;

    protected Vector3 baseScale;

    protected virtual void Awake()
    {
        GameState.OnSpeedChanged += speedChange;
        baseScale = transform.localScale;
    }

    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb2d = GetComponent<Rigidbody2D>();
        DoReset();
    }

    void speedChange()
    {
        velocity = initialVelocity * GameState.GameSpeedScalar;
        if (rb)
            rb.velocity = velocity;
        if (rb2d)
            rb2d.velocity = velocity;
    }

    protected virtual void DoReset()
    {
        rotationalVelocity = Utilities.RandomVector(RotationalVelocityMin, RotationalVelocityMax);
        initialVelocity = Utilities.RandomVector(VelocityMin, VelocityMax);
        velocity = initialVelocity * GameState.GameSpeedScalar;
        if (rb)
        {
            rb.angularVelocity = rotationalVelocity;
            rb.velocity = velocity;
        }
        if (rb2d)
        {
            rb2d.angularVelocity = rotationalVelocity.z;
            rb2d.velocity = velocity;
        }
    }
}
