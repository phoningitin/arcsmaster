﻿using UnityEngine;
using System.Collections;
using System;

public class Detachable : MonoBehaviour
{
    Transform cachedParent = null;
    Action onFinished;

    public void DetachAndWait(float waitTime, Action executeOnReattached)
    {
        cachedParent = transform.parent;
        transform.parent = null;
        onFinished = executeOnReattached;
        StartCoroutine(reattach(waitTime));
    }

    IEnumerator reattach(float time)
    {
        yield return new WaitForSeconds(time);
        onFinished();
        transform.parent = cachedParent;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }
}
