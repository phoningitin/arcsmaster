﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class SelectDifficulty : MonoBehaviour
{
    Dropdown drop;
    public GameState.Difficulty Difficulty;
    public List<GameState.Difficulty> DifficultyList;

#if UNITY_EDITOR
    void OnValidate()
    {
        if (Application.isPlaying)
            return;
        drop = GetComponent<Dropdown>();
        DifficultyList = new List<GameState.Difficulty>();
        for(int i = 0; i< System.Enum.GetValues(typeof(GameState.Difficulty)).Length; i++)
        {
            DifficultyList.Add((GameState.Difficulty)i);
        }
        if(drop)
            drop.options = new List<Dropdown.OptionData>(DifficultyList.Select(diff => new Dropdown.OptionData(diff.ToString())));
    }
#endif

    void Awake()
    {
        drop = GetComponent<Dropdown>();
        Difficulty = (GameState.Difficulty)System.Enum.Parse((typeof(GameState.Difficulty)), PlayerPrefs.GetString("DifficultySetting", "Normal"));
        Debug.Log("loaded diff choice: " + Difficulty.ToString());
        if (drop)
            drop.value = drop.options.FindIndex(op => op.text == Difficulty.ToString());
        if (GetComponent<GameState>())
            GetComponent<GameState>().SetDifficulty(Difficulty);
    }

    public void OnValueChanged(int choice)
    {
        Difficulty = (GameState.Difficulty)choice;
        Debug.Log("saving diff choice: " + Difficulty.ToString());
        PlayerPrefs.SetString("DifficultySetting", Difficulty.ToString());
        PlayerPrefs.Save();
    }
}
