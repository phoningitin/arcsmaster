﻿using UnityEngine;
using System.Collections;

public class Linkable : MonoBehaviour
{
    public Transform nextLinkPosition;

    public void SetPosition(GameObject previousLinkable)
    {
        transform.position = previousLinkable.GetComponent<Linkable>().nextLinkPosition.position;
    }
}
