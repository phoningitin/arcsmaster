﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowTempo : MonoBehaviour
{
    public MidiSeqKaraokeScript midiPlayer;
    Text bpmText;

    void Awake()
    {
        bpmText = GetComponent<Text>();
    }

    void Update()
    {
        bpmText.text = ((int)(midiPlayer.tempo * GameState.GameSpeedScalar)).ToString() + " BPM";
    }
}
