﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ForieroEngine.MIDIUnified.Midi;

public class StartMusic : MonoBehaviour
{
    public AudioSource src;
    public bool startImmediately = false;
    public bool startByDelay = false;
    public MidiSeqKaraokeScript msks;

    public void Start()
    {
        if (startImmediately)
            DoStartMusic(GetDelay()*2);
    }

    ulong GetDelay()
    {
        if (!startByDelay)
            return 0;
        List<MidiEvent> mevList = msks.tracks.SelectMany(trk => trk).Where(mev => MidiEvent.IsNoteOn(mev) && ((NoteEvent)mev).Velocity > 40).ToList();
        mevList.Sort((mev1, mev2) => mev1.AbsoluteTime.CompareTo(mev2.AbsoluteTime));
        long minPlay = mevList.First().AbsoluteTime;
        for (int i = 0; i < 5; i++)
            Debug.Log(mevList[i].ToString());
        Debug.Log(minPlay.ToString());
        return (ulong)minPlay;
    }
    
    void DoStartMusic(ulong d)
    {
        src.Play(d);
        GameObject.Destroy(gameObject);
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.name.Contains("Note") && !startImmediately)
            DoStartMusic(GetDelay());
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.name.Contains("Note") && !startImmediately)
            DoStartMusic(GetDelay());
    }
}
