﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
//using System.Linq;
#if UNITY_EDITOR
using NAudio.Midi;
#endif
using System.Linq;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace PII
{
    public enum MidiCommandCode : byte
    {
        NoteOff = (byte)128,
        NoteOn = (byte)144,
        KeyAfterTouch = (byte)160,
        ControlChange = (byte)176,
        PatchChange = (byte)192,
        ChannelAfterTouch = (byte)208,
        PitchWheelChange = (byte)224,
        Sysex = (byte)240,
        Eox = (byte)247,
        TimingClock = (byte)248,
        StartSequence = (byte)250,
        ContinueSequence = (byte)251,
        StopSequence = (byte)252,
        AutoSensing = (byte)254,
        MetaEvent = (byte)255,
    }

    [System.Serializable]
    public class MidiEventSequence
    {
        public override string ToString()
        {
            return String.Join("\n", events.Select(x => x.ToString()).ToArray());
        }
        public List<PIIEvent> events = new List<PIIEvent>();
#if UNITY_EDITOR
        public MidiEventSequence(List<NoteEvent> combinedEvents, int tempo)
        {
            foreach (NoteEvent evt in combinedEvents)
                events.Add(new PIIEvent(evt, tempo));
        }
#endif
        public MidiEventSequence()
        {

        }
        public MidiEventSequence(List<PIIEvent> fromFile)
        {
            events = fromFile;
        }

        [System.Serializable]
        public class PIIEvent
        {
            public override string ToString()
            {
                return timeMarker.ToString() + ": " + NoteName + ", vel:" + velocity;
            }
            public long absoluteTime;
            public int tempo;
            public float timeMarker { get { return absoluteTime / (tempo * 2f); } }
            public int noteNumber;
            public int velocity;
            public int Channel;
            public MidiCommandCode code;

            private readonly string[] NoteNames = new string[12] { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };

            public PIIEvent() { }
#if UNITY_EDITOR

            public PIIEvent(NoteEvent evt, int tempo)
            {
                this.tempo = tempo;
                if (evt == null)
                    return;
                this.absoluteTime = evt.AbsoluteTime;
                this.code = (MidiCommandCode)((int)evt.CommandCode);
                this.noteNumber = evt.NoteNumber;
                this.velocity = evt.Velocity;
                this.Channel = evt.Channel;
            }

#endif

            public string NoteName
            {
                get
                {
                    if (this.Channel == 16 || this.Channel == 10)
                    {
                        switch (this.noteNumber)
                        {
                            case 35: return "Acoustic Bass Drum";
                            case 36: return "Bass Drum 1";
                            case 37: return "Side Stick";
                            case 38: return "Acoustic Snare";
                            case 39: return "Hand Clap";
                            case 40: return "Electric Snare";
                            case 41: return "Low Floor Tom";
                            case 42: return "Closed Hi-Hat";
                            case 43: return "High Floor Tom";
                            case 44: return "Pedal Hi-Hat";
                            case 45: return "Low Tom";
                            case 46: return "Open Hi-Hat";
                            case 47: return "Low-Mid Tom";
                            case 48: return "Hi-Mid Tom";
                            case 49: return "Crash Cymbal 1";
                            case 50: return "High Tom";
                            case 51: return "Ride Cymbal 1";
                            case 52: return "Chinese Cymbal";
                            case 53: return "Ride Bell";
                            case 54: return "Tambourine";
                            case 55: return "Splash Cymbal";
                            case 56: return "Cowbell";
                            case 57: return "Crash Cymbal 2";
                            case 58: return "Vibraslap";
                            case 59: return "Ride Cymbal 2";
                            case 60: return "Hi Bongo";
                            case 61: return "Low Bongo";
                            case 62: return "Mute Hi Conga";
                            case 63: return "Open Hi Conga";
                            case 64: return "Low Conga";
                            case 65: return "High Timbale";
                            case 66: return "Low Timbale";
                            case 67: return "High Agogo";
                            case 68: return "Low Agogo";
                            case 69: return "Cabasa";
                            case 70: return "Maracas";
                            case 71: return "Short Whistle";
                            case 72: return "Long Whistle";
                            case 73: return "Short Guiro";
                            case 74: return "Long Guiro";
                            case 75: return "Claves";
                            case 76: return "Hi Wood Block";
                            case 77: return "Low Wood Block";
                            case 78: return "Mute Cuica";
                            case 79: return "Open Cuica";
                            case 80: return "Mute Triangle";
                            case 81: return "Open Triangle";
                            default: return string.Format("Drum {0}", (object)this.noteNumber);
                        }
                    }
                    else
                    {
                        int num = this.noteNumber / 12;
                        return string.Format("{0}{1}", (object)NoteNames[this.noteNumber % 12], (object)num);
                    }
                }
            }
        }
    }
    
    public class MIDIsequence : MonoBehaviour
    {
        public List<TextAsset> jsonSongAssets;

        private static MIDIsequence _inst;

        void Awake()
        {
            _inst = this;
        }

        void OnDestroy()
        {
            _inst = null;
        }


        public static MidiEventSequence getEventSequence(string songName)
        {
            if (_inst == null)
                return null;
            string s = _inst.jsonSongAssets.Where(asset => asset.name.Contains(songName)).First().name;
            string path = Application.dataPath + "/Resources/" + s + ".json";
            Debug.Log("searching path: " + path);
            MidiEventSequence recoveredPiiEvents = SerializeXML.DeserializeObject<MidiEventSequence>(path);
            return recoveredPiiEvents;
        }

        #region EditorOnlyArea
#if UNITY_EDITOR

        public UnityEngine.Object midiLocation;
        public bool Process = false;
        public NAudio.Midi.MidiFile file;
        void OnValidate()
        {
            if (Process)
            {
                processMIDI();
                Process = false;
            }
        }

        string retrieveTempoString(MidiEvent evt)
        {
            string temp = evt.ToString().Split(new string[] { "SetTempo" }, StringSplitOptions.None)[1].Trim();
            string str = temp.Remove(temp.IndexOf("bpm"));
            Debug.Log(str);
            return str;
        }

        void processMIDI()
        {
            Debug.Log("searching");
            file = new MidiFile(Application.dataPath.Remove(Application.dataPath.IndexOf("Assets")) + AssetDatabase.GetAssetPath(midiLocation));
            List<MidiEvent> tempoEvents = file.Events.SelectMany(eventList => eventList.Where(evt => evt.CommandCode == NAudio.Midi.MidiCommandCode.MetaEvent && evt.ToString().Contains("SetTempo"))).ToList();
            List<int> tempos = tempoEvents.Select(evt => Convert.ToInt32(retrieveTempoString(evt))).ToList();
            int tempo;
            switch (tempos.Count)
            {
                case 0:
                    Debug.Log("no tempo found, abort!");
                    return;
                case 1:
                    tempo = tempos.First();
                    Debug.Log("tempo: " + tempo.ToString() + "bpm");
                    break;
                default:
                    Debug.Log("multiple tempos, unsupported!");
                    tempo = tempos.First();
                    break;
            }
            MidiEventSequence piiEvents;
            List<MidiEventSequence.PIIEvent> combinedEvents = new List<MidiEventSequence.PIIEvent>();
            for (int i = 0; i < file.Events.Count(); i++)
            {
                List<MidiEvent> mevList = file.Events.ElementAt(i).ToList();
                foreach (NoteEvent m in mevList.Where(mev => mev.CommandCode == NAudio.Midi.MidiCommandCode.NoteOn || mev.CommandCode == NAudio.Midi.MidiCommandCode.NoteOff))
                    if (m != null)
                        combinedEvents.Add(new MidiEventSequence.PIIEvent(m, tempo));
            }
            combinedEvents.Sort((evt1, evt2) => evt1.absoluteTime.CompareTo(evt2.absoluteTime));
            piiEvents = new MidiEventSequence(combinedEvents);

            string path = Application.dataPath + "/Resources/" + midiLocation + ".json";
            SerializeXML.SerializeObject(path, piiEvents);
        }
#endif
        #endregion
    }

}
