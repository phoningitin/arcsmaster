﻿using UnityEngine;
using System.Collections;

public class AudioManipulation : MonoBehaviour
{
    void Awake()
    {
        GameState.OnSpeedChanged += ChangeFrequency;
    }

    void ChangeFrequency()
    {
        GetComponent<AudioSource>().pitch = GameState.GameSpeed; //!= 0f ? Mathf.Pow(GameState.GameSpeed, 0.33f) : 0.1f;
    }
}
