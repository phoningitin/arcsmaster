﻿using UnityEngine;
using System.Collections;

public class DestroyOnCollision : MonoBehaviour
{
    void OnTriggerEnter(Collider c)
    {
        PooledObjects.Destroy(c.gameObject);
    }
    void OnTriggerEnter2D(Collider2D c)
    {
        PooledObjects.Destroy(c.gameObject);
    }
}
