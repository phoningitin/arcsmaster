﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class Explode : MonoBehaviour
{
    public bool Test = false;
    public bool Reset = false;
    public ParticleSystem manipulated;

    float emissionRate = 0f;
    float startSpeed = 0f;
    float startLifetime = 0f;
    float startSize = 1f;

    Obstacle obstacle;

    public List<GameObject> toDeactivate;

    bool canExplode = true;

    void Awake()
    {
        startSpeed = manipulated.startSpeed;
        startLifetime = manipulated.startLifetime;
        emissionRate = manipulated.emissionRate;
        startSize = manipulated.startSize;
        obstacle = GetComponent<Obstacle>();
    }
    public bool IsCowBell = false;
    void OnTriggerEnter(Collider c)
    {
        if(canExplode && c.tag == "Player")
        {
            Execute();
            if (IsCowBell)
            {
                c.SendMessage("OnHitCowbell");
                GameState.GetInst().CowbellBasePitchY = this.transform.position.y;
            }
            else
                c.SendMessage("OnHitExplosion");
        }
    }

    void OnValidate()
    {
        if(Test)
        {
            Execute();
            Test = false;
        }
        if(Reset)
        {
            DoReset();
            Reset = false;
            obstacle.SetReady();
        }
    }

    void OnEnable()
    {
        manipulated.Play();
    }

    void Execute()
    {
        canExplode = false;
        manipulated.startSpeed *= 10f;
        manipulated.startLifetime *= 1f;
        manipulated.startSize *= 5f;
        manipulated.emissionRate = 500;
        StartCoroutine(stopEmit());
        Detachable dt = manipulated.GetComponent<Detachable>();
        if(dt)
        {
            obstacle.SetUnready();
            dt.DetachAndWait(manipulated.startLifetime, obstacle.SetReady);
        }
        foreach (GameObject obj in toDeactivate)
            obj.SetActive(false);
    }

    IEnumerator stopEmit()
    {
        yield return new WaitForSeconds(0.2f);
        manipulated.emissionRate = 0;
    }

    void DoReset()
    {
        canExplode = true;
        manipulated.startSpeed = startSpeed;
        manipulated.startLifetime = startLifetime;
        manipulated.emissionRate = emissionRate;
        manipulated.startSize = startSize;
        foreach (GameObject obj in toDeactivate)
            obj.SetActive(true);
    }
}
