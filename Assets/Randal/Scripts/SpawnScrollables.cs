﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SpawnScrollables : MonoBehaviour
{
    public string itemName;
    public float defaultGenerationSpeed;
    public bool useGameSpeedScalar;
    public bool useRandomColor = false;
    public Color minColor;
    public Color maxColor;
    public bool scaleUniformly = true;
    public Vector3 minScale = Vector3.one;
    public Vector3 maxScale = Vector3.one;
    private BoxCollider boxCollider;
    private Bounds genArea;
    private float timer = 0f;

    public List<GameObject> ActiveScrollables { get { return PooledObjects.GetActiveObjectsByName(itemName); } }

    public void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
        if (boxCollider)
            genArea = boxCollider.bounds;
        if(defaultGenerationSpeed > 0f)
        {
            generate();
            StartCoroutine(generateLoop());
        }
    }

    IEnumerator generateLoop()
    {
        while (true)
        {
            float waitTime = defaultGenerationSpeed * (useGameSpeedScalar ? 1f/GameState.GameSpeedScalar : 1f);
            if(timer >= waitTime)
            {
                timer = 0f;
                generate();
            }
            timer += Time.deltaTime;
            yield return null;
        }
    }

    public GameObject generate(Vector3 pos, Vector3 scale, Color color)
    {
        GameObject generated = PooledObjects.Instantiate(itemName, pos, Quaternion.identity);
        generated.transform.localScale = Vector3.Scale(generated.transform.localScale, scale);
        if (GetComponent<MeshRenderer>())
            GetComponent<MeshRenderer>().material.SetColor("_Color", color);
        else if (GetComponent<SpriteRenderer>())
            GetComponent<SpriteRenderer>().color = color;
        return generated;
    }

    public void generate()
    {
        Vector3 position;
        if (boxCollider != null)
            position = Utilities.RandomVector(genArea.min, genArea.max);//transform.position + 
        else
            position = transform.position;
        Vector3 scale;
        if (!scaleUniformly)
            scale = Utilities.RandomVector(minScale, maxScale);
        else
            scale = Utilities.RandomScaleVector(minScale.x, maxScale.x);
        Color c = Utilities.RandomColor(minColor, maxColor);
        generate(position, scale, c);
    }
}
