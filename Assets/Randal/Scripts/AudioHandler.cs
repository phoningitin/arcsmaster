﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class AudioHandler : MonoBehaviour
{
    public bool invertPitchShiftForNoReason = false;
    public int pregenerateSources = 10;
    List<AudioSource> sources = new List<AudioSource>();
    void Awake()
    {
        for(int i = 0; i< pregenerateSources; i++)
        {
            sources.Add(new GameObject().AddComponent<AudioSource>());
        }
    }
    void Update()
    {
        foreach (AudioSource src in sources)
            if (!src.isPlaying)
                src.gameObject.SetActive(false);
    }
    internal void PlayEffect(AudioClip sample, float volume, int frequencyDifference)
    {
        if (volume < 0.6f)
            return;
        AudioSource src = null;
        for (int i = 0; i< sources.Count; i++)
            if(!sources[i].isPlaying)
            {
                src = sources[i];
                src.gameObject.SetActive(true);
                break;
            }
        if(src == null)
        {
            sources.Add(new GameObject().AddComponent<AudioSource>());
            src = sources.Last();
        }

        //if (volume < 0.5f)
        //    return;
        //creates a fuckton of gameobjects.
        src.loop = false;
        
        //total guess here. i just randomly decided that an octave (11 integer levels) increased pitch by 2x.
        //EDIT: the internet says I picked the right number?
        float fd = (float)frequencyDifference / 11f;
        if (invertPitchShiftForNoReason)
            fd = -fd;
        src.pitch = Mathf.Pow(2f, fd);
        src.PlayOneShot(sample, volume);
    }
}
