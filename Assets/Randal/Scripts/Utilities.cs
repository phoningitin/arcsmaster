﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public static class Utilities
{
    public static Vector3 RandomVector(Vector3 min, Vector3 max)
    {
        return new Vector3(
            Random.Range(min.x, max.x),
            Random.Range(min.y, max.y),
            Random.Range(min.z, max.z));
    }

    public static Color RandomColor(Color minColor, Color maxColor)
    {
        return new Color(
            Random.Range(minColor.r, maxColor.r),
            Random.Range(minColor.g, maxColor.g),
            Random.Range(minColor.b, maxColor.b),
            Random.Range(minColor.a, maxColor.a));
    }

    public static Vector3 RandomScaleVector(float x1, float x2)
    {
        float val = Random.Range(x1, x2);
        return new Vector3(val,val, val);
    }

    public static float CalculateStdDev(IEnumerable<float> values)
    {
        float ret = 0;
        if (values.Count() > 0)
        {
            //Compute the Average      
            float avg = values.Average();
            //Perform the Sum of (value-avg)_2_2      
            float sum = values.Sum(d => Mathf.Pow((float)(d - avg), 2));
            //Put it all together      
            ret = Mathf.Sqrt((float)((sum) / (values.Count() - 1)));
        }
        return ret;
    }
}
