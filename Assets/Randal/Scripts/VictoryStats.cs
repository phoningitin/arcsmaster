﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

public class VictoryStats : MonoBehaviour
{
    public Text CompletionText;
    public Text RankText;
    public Text FlavorText;

    void OnEnable()
    {
        SetStats();
    }
    public static int MaxHighScores = 5;
    void SetStats()
    {
        //PlayerPrefs.SetString("SKIN", skinName);
        //
        int yourscore = ScoreKeeper.Score;
        int perfectscore = (int)((float)((ScoreKeeper.getInst().NotesHit+ ScoreKeeper.getInst().NotesMissed))*100f);
        CompletionText.text = (ScoreKeeper.CompletionPercentage*100f).ToString("F0")+"% Performance";
        string rank = GetRank(ScoreKeeper.CompletionPercentage, GameState.SongDifficulty);
        RankText.text = rank + " Rank";
        FlavorText.text = GetFlavorText(rank);

        int highscoreindex = -1;
        int maxhighscores = MaxHighScores;
        NewHighScore = false;
        for (int i = (maxhighscores - 1); i >= 0; i--)
        {
            int oldscore = PlayerPrefs.GetInt("HighScore");
            if (yourscore > oldscore) highscoreindex = i;
        }

        prepareHighScores();
        PlayerPrefs.SetInt("highscoreindex", highscoreindex); 
        if (highscoreindex != -1)
        {
            for (int i = (maxhighscores - 1); i >= 0; i--)
            {
                if (i > highscoreindex)
                {
                    //swap
                    PlayerPrefs.SetInt("HighScore" + i, PlayerPrefs.GetInt("HighScore"+(i-1)));
                    PlayerPrefs.SetString("HighDifficulty" + i, PlayerPrefs.GetString("HighDifficulty" + (i - 1)));
                    PlayerPrefs.SetString("HighSong" + i, PlayerPrefs.GetString("HighSong" + (i - 1)));
                    PlayerPrefs.SetString("HighCharacter" + i, PlayerPrefs.GetString("HighCharacter" + (i - 1)));
                }
                else if (i == highscoreindex)
                {
                    //set
                    PlayerPrefs.SetInt("HighScore"+i,yourscore);
                    PlayerPrefs.SetString("HighDifficulty"+i,GameState.SongDifficulty.ToString());
                    PlayerPrefs.SetString("HighSong" + i, PlayerPrefs.GetString("SongName"));
                    PlayerPrefs.SetString("HighCharacter" + i, PlayerPrefs.GetString("SKIN"));
                }
            }

            PlayerPrefs.Save();
        }
        int cheervalue = 1;
        switch (rank)
        {
            case "SSS": { cheervalue = 5; break; }
            case "SS": { cheervalue = 5; break; }
            case "S": { cheervalue = 5; break; }
            case "A": { cheervalue = 5; break; }
            case "B": { cheervalue = 4; break; }
            case "C": { cheervalue = 3; break; }
            case "D": { cheervalue = 2; break; }
            case "F": { cheervalue = 1; break; }
        }

        AudioCarrier.getInst().playAudienceSound(cheervalue);
    }

    public static void prepareHighScores()
    {
        if (PlayerPrefs.GetString("setuphighscores", "null").Equals("null"))
        {

            for (int i = 0; i < MaxHighScores; i++)
            {
                PlayerPrefs.SetInt("HighScore" + i, 0);
                PlayerPrefs.SetString("HighDifficulty" + i, "-----");
                PlayerPrefs.SetString("HighSong" + i, "--");
                PlayerPrefs.SetString("HighCharacter" + i, "-------------");
            }
            PlayerPrefs.SetInt("highscoreindex", -1);
            PlayerPrefs.SetString("setuphighscores", "ready");
        }
    }
    public static bool NewHighScore = false;
    string GetFlavorText(string rank)
    {
        switch (rank)
        {
            case "SSS":         return "You broke the game! This should be impossible!";
            case "SS":          return "Spinning faster than time itself!";
            case "S":           return "Follow the beat of your burning heart! <3";
            case "A":           return "The rhythm of your heart is crying out!";
            case "B":           return "Just spin a little harder! You're almost there!";
            case "C":           return "You shine so brightly! Reach for the stars!";
            case "D":           return "...It's hard for me to say this, Clyde, but that sounded like ass.";
            case "F":           return "...Well, you tried...¯\\_(ツ)_/¯";
            default:            return "Oops, your rank is bugged!";
        }
    }

    string GetRank(float completePct, GameState.Difficulty difficulty)
    {
        switch(difficulty)
        {
            case GameState.Difficulty.Easy:
                if (completePct > 0.99f)
                    return "A";
                else if (completePct > 0.98f)
                    return "B";
                else if (completePct > 0.97f)
                    return "C";
                else if (completePct > 0.95f)
                    return "D";
                else
                    return "F";
            case GameState.Difficulty.Normal:
                if (completePct > 0.95f)
                    return "S";
                else if (completePct > 0.9f)
                    return "A";
                else if (completePct > 0.8f)
                    return "B";
                else if (completePct > 0.7f)
                    return "C";
                else if (completePct > 0.6f)
                    return "D";
                else
                    return "F";
            case GameState.Difficulty.Hard:
                if (completePct > 0.95f)
                    return "SS";
                else if (completePct > 0.9f)
                    return "S";
                else if (completePct > 0.8f)
                    return "A";
                else if (completePct > 0.75f)
                    return "B";
                else if (completePct > 0.6f)
                    return "C";
                else if (completePct > 0.5f)
                    return "D";
                else
                    return "F";
            case GameState.Difficulty.Literal:
                if (completePct > 0.9f)
                    return "SSS";
                else if (completePct > 0.8f)
                    return "SS";
                else if (completePct > 0.7f)
                    return "S";
                else if (completePct > 0.6f)
                    return "A";
                else if (completePct > 0.5f)
                    return "B";
                else if (completePct > 0.4f)
                    return "C";
                else if (completePct > 0.3f)
                    return "D";
                else
                    return "F";
            default:
                return "F";
        }
    }
}
