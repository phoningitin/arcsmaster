﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

//AUTHOR: RANDAL
//we use a pregenerated pool of objects for things that are created all the time.
//create them all at once, then merely activate them on command, and return them to the pool when they would be destroyed.
//much more efficient, as far as Unity is concerned.
//generic utility class
public class PooledObjects : MonoBehaviour
{
    [System.Serializable] public class PooledType
    {
        public string name;
        public GameObject prefab;
        public int numberOfCopies;
    }

    public static List<GameObject> GetActiveObjectsByName(string itemName)
    {
        if (_inst != null && _inst.pools.ContainsKey(itemName))
            return _inst.pools[itemName].Where(obj => obj.activeSelf).ToList();
        else
            return null;
    }

    public List<PooledType> poolInfoList;
    Dictionary<string, List<GameObject>> pools = new Dictionary<string, List<GameObject>>();
    private static PooledObjects _inst;
    Dictionary<string, GameObject> lastCreated = new Dictionary<string, GameObject>();
    

    void Awake()
    {
        _inst = this;
        foreach (PooledType pt in poolInfoList)
        {
            pools.Add(pt.name, CreatePool(pt.prefab, pt.numberOfCopies));
            lastCreated.Add(pt.name, null);
        }
    }
    
    void OnDestroy()
    {
        _inst = null;
    }

    private List<GameObject> CreatePool(GameObject prefab, int numberOfCopies)
    {
        List<GameObject> generated = new List<GameObject>();
        for (int i = 0; i < numberOfCopies; i++)
        {
            generated.Add(GameObject.Instantiate(prefab) as GameObject);
            generated.Last().name += "_"+i.ToString();
            resetObject(generated.Last());
        }
        return generated;
    }

    void resetObject (GameObject toReset)
    {
        toReset.SendMessage("DoReset", SendMessageOptions.DontRequireReceiver);
        toReset.SetActive(false);
        toReset.transform.parent = transform;
        toReset.transform.localPosition = Vector3.zero;
        toReset.transform.localRotation = Quaternion.identity;
    }

    public static GameObject Instantiate(string ObjectName, Vector3 position, Quaternion rotation)
    {
        if (_inst != null)
            return _inst._instantiate(ObjectName, position, rotation);
        else return null;
    }

    private GameObject _instantiate(string ObjectName, Vector3 position, Quaternion rotation)
    {
        if (!pools.ContainsKey(ObjectName))
            return null;
        //get the next available object in the pool.
        //checks to find the first inactive object. if that inactive object is an "obstacle", check to see if that obstacle
        //is also "ready". if not ready, keep looking.
        //unready obstacles include obstacles with detached particle systems that are still visible and active. Such obstacles
        //will return to being ready once the particle systems reattach themselves. Until then, use a different object.
        GameObject available = pools[ObjectName].Find(obj => 
                !obj.activeSelf && 
                (obj.GetComponent<Obstacle>() ? obj.GetComponent<Obstacle>().IsReady : true)
            );
        if (available == null)
        {
            GameObject generateNew = GameObject.Instantiate(poolInfoList.Find(info => info.name == ObjectName).prefab, position, rotation) as GameObject;
            pools[ObjectName].Add(generateNew);
            lastCreated[ObjectName] = generateNew;
            return generateNew;
        }
        else
        {
            available.SetActive(true);
            available.SendMessage("DoReset", SendMessageOptions.DontRequireReceiver);
            available.transform.rotation = rotation;
            if (available.GetComponent<Linkable>() && lastCreated[ObjectName] != null)
                available.GetComponent<Linkable>().SetPosition(lastCreated[ObjectName]);
            else
                available.transform.position = position;
            lastCreated[ObjectName] = available;
            return available;
        }
    }

    //obviously we don't actually destroy the object - we just hide it. I use the same word as unity to make it clear they should be treated as the same command logically (but this way is more efficient)
    public static void Destroy(GameObject toDestroy)
    {
        _inst.resetObject(toDestroy);
    }
}
