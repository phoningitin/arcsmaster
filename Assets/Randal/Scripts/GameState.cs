﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class GameState : MonoBehaviour
{
    public LoopTexture scaleLooper;
    public Slider speedSlider;
    public delegate void SimpleAction();
    public static SimpleAction OnSpeedChanged;
    public static SimpleAction OnWin;
    public static SimpleAction OnFail;
    public static SimpleAction OnPause;
    public static SimpleAction OnResume;
    public static SimpleAction OnNormalMotion;
    public static SimpleAction OnSlowMotion;
    public static SimpleAction OnFastMotion;
    private float _currentSpeed = 1f;
    private bool _paused = false;
    public GameObject UI_pause;
    public enum Difficulty { Easy, Normal, Hard, Literal };
    private Difficulty _songDifficulty = Difficulty.Easy;
    public static Difficulty SongDifficulty { get { if (_inst != null) return _inst._songDifficulty; else return Difficulty.Normal; } }
    [System.Serializable] public class DifficultySetting
    {
        [HideInInspector] public string name;
        public float NoteInterval;
    }
    [SerializeField] List<DifficultySetting> difficultySettings;

#if UNITY_EDITOR
    void OnValidate()
    {
        int difficultyCount = System.Enum.GetValues(typeof(GameState.Difficulty)).Length;
        if (!Application.isPlaying && (difficultySettings == null || difficultySettings.Count != difficultyCount))
        {
            difficultySettings = new List<DifficultySetting>();
            for (int i = 0; i < difficultyCount; i++)
            {
                difficultySettings.Add(new DifficultySetting());
                difficultySettings.Last().name = ((Difficulty)i).ToString();
            }
        }
    }
#endif

    public enum State { BeforeStart, SlowMo, InProgress, Failed, Victory }
    private State _currentState;
    public static State CurrentState { get { if (_inst == null) return State.BeforeStart; else return _inst._currentState; } }
    
    public GameObject player;

    //1% per second
    public float speedDecayRate = 0.01f, SpeedBoostIncrement = 0.01f;

    public float initialSpeed = 1f;
    public float maxSpeed = 2f;
    public float LastTrailNoteHitTime = 0f;
    public float LastTrailNoteSlowDownAfterTime = 1f;
    public FlameBar GameFlameBar;
    public float SlowMoResource = 0f;
    public float SlowMoDuration = 5f;
    public float SlowMoRegenTime = 20f;
    public float SlowMoCapacity = 1f;
    public bool CowbellBonus = false;
    public float CowbellBasePitchY = 0f;
    public float CowbellsPerMinute = 4f;
    public float LastCowbellTime = -1f;
    public bool SpawnCowbell = false;
    public void SetDifficulty(Difficulty difficulty)
    {
        _songDifficulty = difficulty;
    }

    public void OnHitCowbell()
    {
        if (!CowbellBonus)
        {
            CowbellBonus = true;
            //Show some UI that indicates an increase in score.
            AudioCarrier.getInst().playCowbellSound();
        }
    }

    public static float BaseNoteInterval { get { if (_inst != null) return _inst.difficultySettings[(int)_inst._songDifficulty].NoteInterval; else return 0f; } }

    public static float NoteInterval { get { return BaseNoteInterval / GameSpeed; } }

    public static float GameSpeed { get { return _inst._currentSpeed; } }

    public static float GameSpeedScalar
    {
        get {
            return _inst._currentSpeed / _inst.initialSpeed;
        }
    }

    public static float MaxSpeed { get { return _inst.maxSpeed; } }
    public static void addSlowMo(float sm) { _inst.SlowMoResource = Mathf.Clamp01(_inst.SlowMoResource+(sm/_inst.SlowMoCapacity)); }
    public static void reduceSlowMo(float sm) { _inst.SlowMoResource = Mathf.Clamp01(_inst.SlowMoResource - (sm / _inst.SlowMoCapacity)); }
    private float _lastSpeed = 1f;
    private static GameState _inst;
    public MidiSeqKaraokeScript sequencer;
    public bool AutoVelocity=false;

    private bool shittySolution = false;

    private bool finishedFlag = false;

    void Awake()
    {
        _inst = this;
        Application.targetFrameRate = 90;
        OnSpeedChanged += () => { };

        LastTrailNoteHitTime = Time.time;
        LastTomatoThrowTime = -1f;

        OnWin += WinGame;
        OnFail += LoseGame;
        OnPause += () => { };
        OnResume += () => { };
        OnNormalMotion += () => { };
        OnSlowMotion += () => { };
        OnFastMotion += () => { };
    }

    void Start()
    {
        _currentSpeed = initialSpeed;
        ChangeGameSpeed();
        player.SetActive(true);
        UI_pause.SetActive(false);
        sequencer.OnFinished += setFinishedFlag;
    }

    void OnDestroy()
    {
        _inst = null;
        OnSpeedChanged -= OnSpeedChanged;
        OnWin -= OnWin;
        OnFail -= OnFail;
        OnPause -= OnPause;
        OnResume -= OnResume;
        OnNormalMotion -= OnNormalMotion;
        OnSlowMotion -= OnSlowMotion;
        OnFastMotion -= OnFastMotion;
    }
    public static void StartSong()
    {
        _inst.startSong();
    }

    private void startSong()
    {
        if (_currentState != State.BeforeStart)
            return;
        _currentState = State.InProgress;

        StartCoroutine(reduceSpeed());
    }

    IEnumerator reduceSpeed()
    {
        while (_currentState == State.InProgress)
        {
            yield return new WaitForSeconds(.25f);
            if (((Time.time - LastTrailNoteHitTime) >= LastTrailNoteSlowDownAfterTime) && (LastTrailNoteSlowDownAfterTime > 0))
            {
                _currentSpeed = Mathf.Clamp(_currentSpeed * (1f - speedDecayRate), 0.01f, maxSpeed);
                ChangeGameSpeed();
            }
        }
    }
    public float getCurrentSpeed()
    {
        return _currentSpeed;
    }
    void LoseGame()
    {
        Time.timeScale = 0f;
        OnSpeedChanged();
        AudioCarrier.getInst().playAudienceSound(0);
        _currentState = State.Failed;
        enabled = false;
    }

    void WinGame()
    {
        Time.timeScale = 0f;
        OnSpeedChanged();
        _currentState = State.Victory;
        enabled = false;
    }
    public float LastTomatoThrowTime = -1f;
    public float TomatoThrowRateAtLowestSpeed = 5f;
    void Update()
    {
        //If pause button is pressed, call the same function (Pause())
        //that is called by the || button at top left.
        //Same function is also called by the "resume" text as well -
        //(but in that case, the game will be paused already of course, so it unpauses)
        if (_currentState == State.SlowMo)
        {

            Time.timeScale = Mathf.Lerp(Time.timeScale,Mathf.Lerp(1f,.5f,SlowMoResource),Time.deltaTime*3f);
            SlowMoResource = Mathf.Clamp01(SlowMoResource-((Time.deltaTime/Time.timeScale)/SlowMoDuration));
            GameFlameBar.Incinerating = 1f;
            if (_currentState == State.SlowMo && (SlowMoResource <= 0f))
            {
                Time.timeScale = 1f;
                AudioCarrier.getInst().stopSlowMoSound();
            }
        } else
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, 1f, Time.deltaTime * 1f);
            //Slowly regenerate slow mo overtime for newbies.
            if (SlowMoResource < .25f)
            SlowMoResource = Mathf.Clamp01(SlowMoResource + (((Time.deltaTime / Time.timeScale)/SlowMoRegenTime))/SlowMoCapacity);
            GameFlameBar.Incinerating = 0f;
            

        }
        GameFlameBar.FlameValue = SlowMoResource;

        if ((_currentState == State.InProgress) || (_currentState == State.SlowMo))
        {
            if ((_currentSpeed < (maxSpeed*.75f)) && ((LastTomatoThrowTime < 0f)||(Time.time - LastTomatoThrowTime) >= (Mathf.Lerp(TomatoThrowRateAtLowestSpeed,0f, Mathf.Clamp01((_currentSpeed)/0.75f)))))
            {
                LastTomatoThrowTime = Time.time;
                SpawnObstacles.spawnTomato();
            }

            if (_currentSpeed >= 1f)
            {
                if ((Time.time - LastCowbellTime) >= (60f/ CowbellsPerMinute)){
                    
                    //SpawnCowbell = true;

                }
                
                
            }
        }
        if (Input.GetButtonDown("Pause"))
            Pause();
        if(shittySolution)
        {
            shittySolution = false;
            sequencer.Continue();
        }
        if (finishedFlag)
            if (!CreateNotes.AreNotesRemaining)
                OnWin();
    }

    private void setFinishedFlag()
    {
        finishedFlag = true;
    }


    public void Pause()
    {
        _paused = !_paused;

        if (_paused)
        {
            Debug.Log("pausing...");
            sequencer.Pause();
            player.SetActive(false);
            Time.timeScale = 0f;
            OnSpeedChanged();
            OnPause();
        }
        if (!_paused)
        {
            Debug.Log("unpausing...");
            sequencer.Continue();
            player.SetActive(true);
            Time.timeScale = 1f;
            OnSpeedChanged();
            OnResume();
            shittySolution = true;
            finishedFlag = false;

            //removed "resumeScrollables" function,
            //since they never technically stopped moving.
            //timescale = 0 doesnt directly pause anything,
            //it pauses time itself!
            //so there's no need to resume anything, I think.
            //except for the sequencer apparently. no idea why.
            //maybe because the synthesizer runs outside of unity time.
        }
    }

    public void goToMainMenu()
    {
        Time.timeScale = 1f;
        Application.LoadLevel(0);
    }

    public void ChangeGameSpeed()
    {
        scaleLooper.levelSpeed = _currentSpeed;
        
        if (sequencer)
            sequencer.SetSpeed(_currentSpeed);
        OnSpeedChanged();
        _lastSpeed = _currentSpeed;
    }

    public static void SpeedBoost()
    {
        _inst._currentSpeed = Mathf.Clamp(_inst._currentSpeed + Mathf.Max(0.01f,_inst.SpeedBoostIncrement), 0f, _inst.maxSpeed);
        _inst.ChangeGameSpeed();
    }

    public static void SpeedHit()
    {
        AudioCarrier.getInst().playSplatSound();
        _inst._currentSpeed = Mathf.Clamp(_inst._currentSpeed - 0.05f, 0f, _inst.maxSpeed);
        _inst.ChangeGameSpeed();
    }

    public static void SpeedMiss()
    {
        _inst._currentSpeed = Mathf.Clamp(_inst._currentSpeed - 0.03f, 0f, _inst.maxSpeed);
        _inst.ChangeGameSpeed();
    }

    public void SlowMo()
    {
        StartCoroutine(slowMotion());
        OnSlowMotion();
    }
    public void SlowMoInput()
    {
        slowMotionInput();
        OnSlowMotion();
    }

    IEnumerator slowMotion()
    {
        Time.timeScale = 0.5f;
        
        _currentState = State.SlowMo;
        yield return new WaitForSeconds(5f);
        _currentState = State.InProgress;
        Time.timeScale = 1f;
        OnNormalMotion();
    }
    public void slowMotionInput()
    {
        Time.timeScale = 0.5f;
        if (_currentState != State.SlowMo) AudioCarrier.getInst().playSlowMoSound();
        _currentState = State.SlowMo;
        OnSlowMotion();
    }
    public void SlowMoInputNormal()
    {
        if (_currentState != State.InProgress) AudioCarrier.getInst().stopSlowMoSound();
        _currentState = State.InProgress;
        Time.timeScale = 1f;
        OnNormalMotion();
        
    }
    public static GameState GetInst()
    {
        return _inst;
    }

    public Difficulty GetDifficulty()
    {
        return _songDifficulty;
    }
}
