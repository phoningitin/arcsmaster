﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ForieroEngine.MIDIUnified;
using System.Linq;
using UnityEngine.UI;
using System;

//WARNING: TEMPORARY
public class CreateNotes : MonoBehaviour
{
    public class CreateNoteNode
    {
        public bool on = true;
        public bool SlowMo = false;
        public int aMidiIndex;
        public int aVolume;
        public int channel;
        public float timeSinceLevelLoad;
        public float timeDifferential;
        public float timeMarker;

        public override string ToString()
        {
            return "Note: " + aMidiIndex.ToString() + ", Volume: " + aVolume.ToString() + ", channel: " + channel.ToString() + ", time: " + timeDifferential.ToString();
        }

        public int MotionCorrectedIndex
        {
            get
            {
                int mod = 0;
                mod += (GameState.CurrentState == GameState.State.SlowMo ? -11 : 0);
                return Mathf.Clamp(aMidiIndex + mod, 0, 127);
            }
        }

        public int SpeedCorrectedIndex
        {
            get
            {
                int mod = 0;
                mod += (int)(Mathf.Log(GameState.GameSpeedScalar, 2f) * 11);
                return Mathf.Clamp(aMidiIndex + mod, 0, 127);
            }
        }

        public float GetPitchNormalized()
        {
            return ((float)(aMidiIndex) / (float)(127));
        }

        public float GetPitchExpanded()
        {
            int midDiff = aMidiIndex - 64;
            int magRange = Mathf.Clamp((int)(midDiff * 1.4f), -64, 63);
            int newHeightIndex = Mathf.Clamp(magRange + 64, 0, 127);
            return (float)newHeightIndex / (float)127;
        }

        public float GetVolumeNormalized()
        {
            return (float)aVolume / (float)127;
        }

        public static float GetPitchExpanded(int aMidiIndex)
        {
            return new CreateNoteNode() { aMidiIndex = aMidiIndex }.GetPitchExpanded();
        }
    }


    public TrailMidpoint prefab;
    public BoxCollider generationArea;
    [Header("Expecting objects that implement IMidiEvents.")]
    public List<MidiSeqKaraokeScript> generators;

    MidiEvents midiEvents;
    List<CreateNoteNode> nodeCreation = new List<CreateNoteNode>();
    TrailNote prevNote = null;

    public Text DebugText;

    private static CreateNotes _inst;

    int notespawnedindex = 0;
    int everyothertest = 0;

    public static bool AreNotesRemaining { get { if (_inst != null) return _inst.GetComponent<SpawnScrollables>().ActiveScrollables.Count > 0; else return false; } }

    

    void Awake()
    {
        _inst = this;
        midiEvents = new MidiEvents();
        foreach (UnityEngine.Object o in generators)
        {
            IMidiEvents ime = o as IMidiEvents;
            if (ime != null)
            {
                midiEvents.AddGenerator(ime);
            }
            else {
                Debug.LogError(o.name + " does not implement IMidiEvents!!!");
            }
        }

        midiEvents.NoteOnEvent += NoteOnHandler;
        midiEvents.NoteOffEvent += NoteOffHandler;
        midiEvents.PedalOnEvent += PedalOn;
        midiEvents.PedalOffEvent += PedalOff;

        StartCoroutine(placeNotesAtInterval());

        Application.logMessageReceived += LogHandler;
    }

    void PedalOn(PedalEnum aPedal, int aValue, int aChannel)
    {
       Debug.Log("Pedal ON : " + aPedal.ToString() + " Channel : " + aChannel);
    }

    void PedalOff(PedalEnum aPedal, int aValue, int aChannel)
    {
        Debug.Log("Pedal ON : " + aPedal.ToString() + " Channel : " + aChannel);
    }

    private void NoteOffHandler(int aMidiId, int aValue, int aChannel)
    {
        nodeCreation.Add(new CreateNoteNode() { on = false, SlowMo = GameState.CurrentState == GameState.State.SlowMo, aMidiIndex = aMidiId, aVolume = aValue, channel = aChannel, timeSinceLevelLoad = Time.timeSinceLevelLoad });
    }

    private void NoteOnHandler(int aMidiIndex, int aVolume, int channel)
    {
        nodeCreation.Add(new CreateNoteNode() { on = true, SlowMo = GameState.CurrentState == GameState.State.SlowMo, aMidiIndex = aMidiIndex, aVolume = aVolume, channel = channel, timeSinceLevelLoad = Time.timeSinceLevelLoad });
    }

    void OnDestroy()
    {
        _inst = null;
    }

    private void LogHandler(string message, string stacktrace, LogType type)
    {
        if(type == LogType.Error || type == LogType.Exception || type == LogType.Assert)
        {
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace();
            DebugText.text = trace.ToString();
        }
        // Now use trace.ToString(), or extract the information you want.
    }

    IEnumerator placeNotesAtInterval()
    {
        while(true)
        {
            yield return new WaitForSeconds(GameState.NoteInterval / GameState.GameSpeedScalar);
            if (nodeCreation.Count > 0)
            {
                //if there is large standard deviation between note pitches, split notes and call twice
                //float stdev = Utilities.CalculateStdDev(nodeCreation.Select(note => note.GetPitchNormalized()));
                //if (nodeCreation.Count > 1 && stdev > 0.08f)
                //{
                //    ////split notes into those above and below group average
                //    //List<CreateNoteNode> highNotes = nodeCreation.Where(note => note.aMidiIndex > nodeCreation.Average(n => n.aMidiIndex)).ToList();
                //    //List<CreateNoteNode> lowNotes = nodeCreation.Where(note => note.aMidiIndex <= nodeCreation.Average(n => n.aMidiIndex)).ToList();
                //    ////read the time that has passed since each note was queued, and average that number for each group
                //    //float highsAverageTimeOffset = highNotes.Average(note => Time.timeSinceLevelLoad - note.timeSinceLevelLoad);
                //    //float lowsAverageTimeOffset = highNotes.Average(note => Time.timeSinceLevelLoad - note.timeSinceLevelLoad);
                //    //if(highsAverageTimeOffset > lowsAverageTimeOffset)
                //    //{
                //    //    handleNotes(lowNotes);
                //    //    handleNotes(highNotes, highsAverageTimeOffset - lowsAverageTimeOffset);
                //    //}
                //    //else
                //    //{
                //    //    handleNotes(highNotes);
                //    //    handleNotes(lowNotes, lowsAverageTimeOffset - highsAverageTimeOffset);
                //    //}
                //}
                //else
                List<CreateNoteNode> onNodes = nodeCreation.Where(node => node.on).ToList();
                if (onNodes.Count > 0)
                    handleNotes(onNodes, nodeCreation);
                else
                    foreach (CreateNoteNode node in nodeCreation)
                        playNote(node);
                nodeCreation.Clear();
            }
        }
    }

    IEnumerator playNote(CreateNotes.CreateNoteNode note)
    {
        yield return new WaitForSeconds(GameState.NoteInterval - note.timeDifferential);
        MidiOut.NoteOff(Mathf.Clamp(note.MotionCorrectedIndex, 0, 127), note.channel);
    }
    public static CreateNotes getInst() { return _inst; }
    public TrailNote getPrevNote()
    {
        return prevNote;
    }

    void handleNotes(List<CreateNoteNode> combineNotes, List<CreateNoteNode> attachNotes, float positionOffset = 0f)
    {
        int aMidiIndex = getWeightedAverageIndex(combineNotes);
        int channel = combineNotes.First().channel;
        int aVolume = Mathf.Clamp((int)combineNotes.Sum(node => node.aVolume), 0, 127);

        float channelFloat = (float)channel / (float)16;
        if ((everyothertest > 0) &&((notespawnedindex % everyothertest)!=0)){
            notespawnedindex++;

        } else {
            GameObject note = GetComponent<SpawnScrollables>().generate(
                calculateNotePosition(aMidiIndex, generationArea.bounds, positionOffset),
                Vector3.one,
                new Color(channelFloat, channelFloat, channelFloat));
            TrailNote newNote = note.GetComponent<TrailNote>();
            newNote.PreviousNote = prevNote;
            newNote.NoteIndex = notespawnedindex++;
            prevNote = newNote;
            newNote.midpointsadded = false;
            Color notecolor = Color.blue;
            GameState.Difficulty SongDifficulty = GameState.GetInst().GetDifficulty();
            
            if (GameState.GetInst().SpawnCowbell)
            {
                GameState.GetInst().LastCowbellTime = Time.time;
                GameState.GetInst().SpawnCowbell = false;
                SpawnObstacles.spawnCowbell(newNote);
            }
            switch (SongDifficulty)
            {
                case GameState.Difficulty.Easy: { notecolor = Color.blue; break; } //Few notes
                case GameState.Difficulty.Normal: { notecolor = Color.yellow; break; } //Some notes
                case GameState.Difficulty.Hard: { notecolor = Color.red; break; } //Many notes
                case GameState.Difficulty.Literal: { notecolor = Color.green; break; } //All notes
            }
            notecolor = Color.Lerp(notecolor, Color.white, .25f);
            newNote.NoteColor = notecolor;
            newNote.FadeAwayValue = 0f;
            newNote.GetComponent<Animator>().SetTrigger("Reset");
            foreach (MeshRenderer mr in newNote.GetComponentsInChildren<MeshRenderer>()) mr.material.color = notecolor;
            newNote.GetComponent<LineRenderer>().material.SetColor("_TintColor",new Color(notecolor.r, notecolor.g, notecolor.b,.5f)); 
            foreach (CreateNoteNode node in nodeCreation)
                node.timeDifferential = Time.timeSinceLevelLoad - node.timeSinceLevelLoad;
            newNote.AttachNotes(attachNotes);
            newNote.addMidpoints();
        }
    }

    int getWeightedAverageIndex(List<CreateNoteNode> nodes)
    {
        List<CreateNoteNode> loud = nodes.Where(note => note.GetVolumeNormalized() > 0.5f).ToList();
        CreateNoteNode highNote = null;
        if (loud.Count > 0)
            highNote = loud.OrderBy(note => note.GetPitchNormalized()).Last();
        if (highNote != null && Utilities.CalculateStdDev(nodes.Select(note => note.GetPitchNormalized())) > 0.07f)
        {
            return highNote.aMidiIndex;
        }
        int totalWeight = nodes.Sum(node => node.aVolume);
        int averageIndex = nodes.Sum(node => (int)((float)node.aMidiIndex * ((float)node.aVolume / (float)totalWeight)));
        return averageIndex;
    }

    Vector3 calculateNotePosition(int aMidiIndex, Bounds generationBounds, float positionOffset)
    {
        Vector3 v = transform.position;
        float height = generationBounds.max.y - generationBounds.min.y;
        v.y = generationBounds.min.y + height * CreateNoteNode.GetPitchExpanded(aMidiIndex);
        v.x += positionOffset;
        return v;
    }
}
