﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerInput : MonoBehaviour
{
    public enum ControlScheme { OnTap, FingerSeeking }
    public ControlScheme controls;
    public enum InputButtonState { UpPressed, DownPressed, NonePressed}
    public InputButtonState btnState = InputButtonState.NonePressed;
    public float JumpPower;
    public float GravityPower;
    public GameObject platformPrefab;
    public Rigidbody rb;
    float texSpeedToVelocityMultiplier = 14f;
    public List<ParticleSystem> quantityAffectedByGameSpeed;
    public List<ParticleSystem> quantityAffectedByComboBonus;
    public List<ParticleSystem> velocityAffectedByGameSpeed;
    public List<ParticleSystem> playOnFail;
    public List<ParticleSystem> playOnVictory;
    Vector3 platformYOffset = new Vector3(0f, 0.5f, 0f);
    GameObject newPlatform;
    float powerTimer = 0f;
    public float defaultRotationSpeed;
    public int TrailNoteCollidedIndex = -1;
    public int MidNoteCollidedIndex = -1;
    public int TrailNoteMidCollidedIndex = -1;
    public int MidNotesCollided = 0;
    public Vector3 fingerSeekingAdder = new Vector3(0f, 1f, 0f);
    public float movementSpeedModifier = 1f;
    public int TrailNoteChain = 0;
    private bool _ignoreButtonPress = false;

    void Start()
    {
        //Physics.gravity = Vector3.zero;
        rb.maxAngularVelocity *= 10f;
        GetComponent<ConstantForce>().force = -Physics.gravity;
        
        GameState.OnSpeedChanged += resetParticles;
        GameState.OnWin += OnWin;
        GameState.OnFail += OnFail;
    }

    void OnTriggerEnter(Collider c)
    {
        if(c.name.Contains("Note"))
        {
            Spin(-0.2f);
        }
        else if(c.name.Contains("Grenade"))
        {
            rb.angularVelocity *= 0.5f;
        }
    }

    public void IgnoreButtonPress()
    {
        _ignoreButtonPress = true;
    }

    public void OnFail()
    {
        foreach (ParticleSystem ps in playOnFail)
            ps.Play();
    }

    public void OnWin()
    {
        foreach (ParticleSystem ps in playOnVictory)
            ps.Play();
    }

    void Update()
    {
        switch(controls)
        {
            case ControlScheme.OnTap:
                UpdateButtonState();
                if (btnState != InputButtonState.NonePressed)
                    powerTimer += Time.deltaTime;
                else
                    powerTimer = 0f;
                break;
            case ControlScheme.FingerSeeking:
                break;
        }
    }

    Vector3 getPlayerFingerPositionWorldSpace()
    {
        return Camera.main.ScreenToWorldPoint(playerFingerPosition);
    }
    Vector3 playerFingerPosition;

    void FixedUpdate()
    {
        if (Input.touches.Length > 0)
            useTouches = true;
        if (controls == ControlScheme.FingerSeeking && (!_ignoreButtonPress && (Input.touches.Length > 0 || Input.GetMouseButton(0))))
        {
            playerFingerPosition = (Input.touches.Length > 0 ? (Vector3)(Input.touches[0].position) : Input.mousePosition);
            rb.velocity += calculateAdder();
            //if (rb.velocity.y > 4f)
            //    Spin(-(rb.velocity.y - 4f) / 100f);
            //else if (rb.velocity.y < -4f)
            //    Spin(-(rb.velocity.y + 4f) / 100f);

            Vector3 equilibriumPosition = getPlayerFingerPositionWorldSpace() - new Vector3(0f, (20f * (GameState.MaxSpeed - GameState.GameSpeed)), 0f);

            GetComponent<ConstantForce>().force = (1f - (transform.position.y - equilibriumPosition.y)) * -Physics.gravity;
        }
        else
            GetComponent<ConstantForce>().force = Vector3.zero;
        if(_ignoreButtonPress)
            if(!Input.GetMouseButton(0) || TouchReleased())
                _ignoreButtonPress = false;
    }

    bool useTouches = false;

    bool TouchReleased()
    {
        if (useTouches && Input.touches.Length == 0)
            return true;
        return false;
    }
    private bool multitouchslow = false;
    Vector3 calculateAdder()
    {
        if (Input.GetMouseButton(0))
        {
            if (Input.GetMouseButton(1)) GameState.GetInst().SlowMoInput(); else GameState.GetInst().SlowMoInputNormal();
            return fingerSeekingAdder * movementSpeedModifier * GameState.GameSpeedScalar * ((Camera.main.ScreenToWorldPoint(Input.mousePosition)).y - transform.position.y);
        }
        else if (Input.touches.Length > 0)
        {
            if (Input.touches.Length > 1)
            {
                if (!multitouchslow) GameState.GetInst().SlowMoInput();
                multitouchslow = true;
            }
            else {
                if (multitouchslow) GameState.GetInst().SlowMoInputNormal(); multitouchslow = false;
            }

            return fingerSeekingAdder * movementSpeedModifier * GameState.GameSpeedScalar * ((Camera.main.ScreenToWorldPoint(Input.touches[0].position)).y - transform.position.y);
        }
        else
        {
            GameState.GetInst().SlowMoInputNormal();
            return Vector3.zero;
        }
    }

    void resetParticles()
    {
        foreach (ParticleSystem ps in velocityAffectedByGameSpeed)
        {
            ParticleSystem.VelocityOverLifetimeModule vlm = ps.velocityOverLifetime;
            ParticleSystem.MinMaxCurve mmc_x = new ParticleSystem.MinMaxCurve();
            mmc_x.constantMin = texSpeedToVelocityMultiplier * -GameState.GameSpeed;
            mmc_x.constantMax = texSpeedToVelocityMultiplier * -GameState.GameSpeed;
            vlm.x = mmc_x;
        }
        foreach (ParticleSystem ps in quantityAffectedByGameSpeed)
        {
            ParticleSystem.EmissionModule em = ps.emission;

            ParticleSystem.MinMaxCurve mmc_rate = new ParticleSystem.MinMaxCurve();
            mmc_rate.constantMin = 1000f * (Mathf.Pow(GameState.GameSpeed / GameState.MaxSpeed, 3f));
            mmc_rate.constantMax = 1000f * (Mathf.Pow(GameState.GameSpeed / GameState.MaxSpeed, 3f));

            em.rate = mmc_rate;
        }
        foreach (ParticleSystem ps in quantityAffectedByComboBonus)
        {
            ParticleSystem.EmissionModule em = ps.emission;

            ParticleSystem.MinMaxCurve mmc_rate = new ParticleSystem.MinMaxCurve();
            mmc_rate.constantMin = 1000f * (Mathf.Pow((float)ScoreKeeper.Combo/100f, 3f));
            mmc_rate.constantMax = 1000f * (Mathf.Pow((float)ScoreKeeper.Combo/100f, 3f));

            em.rate = mmc_rate;
        }
    }

    public void OnHitExplosion()
    {
        GameState.SpeedHit();
    }

    

    public void UpArrowPressed()
    {
        upPressed = true;
    }

    public void UpArrowReleased()
    {
        upReleased = true;
    }

    public void DownArrowPressed()
    {
        downPressed = true;
    }

    public void DownArrowReleased()
    {
        downReleased = true;
    }

    bool upPressed = false;
    bool upReleased = false;
    bool downPressed = false;
    bool downReleased = false;

    void UpdateButtonState()
    {
        if (Input.GetButtonDown("UpArrow"))
            upPressed = true;
        if (Input.GetButtonDown("DownArrow"))
            downPressed = true;
        if (Input.GetButtonUp("UpArrow"))
            upReleased = true;
        if (Input.GetButtonUp("DownArrow"))
            downReleased = true;
        switch (btnState)
        {
            case InputButtonState.NonePressed:
                if (upPressed)
                {
                    btnState = InputButtonState.UpPressed;
                    PreparePlatform();
                    break;
                }
                if (downPressed)
                {
                    btnState = InputButtonState.DownPressed;
                    PreparePlatform();
                    break;
                }
                break;
            case InputButtonState.DownPressed:
                if (downReleased)
                {
                    btnState = InputButtonState.NonePressed;
                    PlatformDown();
                    SpinDown();
                    Jump(-1f);
                    break;
                }
                break;
            case InputButtonState.UpPressed:
                if (upReleased)
                {
                    btnState = InputButtonState.NonePressed;
                    PlatformUp();
                    SpinUp();
                    Jump(1f);
                    break;
                }
                break;
        }
        downPressed = false; downReleased = false; upPressed = false; upReleased = false;
    }

    void PlatformDown()
    {
        Platform();
        newPlatform.transform.position = newPlatform.transform.position + platformYOffset;
    }

    void PlatformUp()
    {
        Platform();
        newPlatform.transform.position = newPlatform.transform.position - platformYOffset;
    }

    void SpinUp() { Spin(60f); }

    void SpinDown() { Spin(-60f); }

    void Spin(float scalar)
    {
        rb.angularVelocity += new Vector3(0f, 0f, scalar * defaultRotationSpeed * texSpeedToVelocityMultiplier * transform.localScale.x * 0.5f);
    }

    void Platform()
    {
        newPlatform = PooledObjects.Instantiate("Platform", transform.position, Quaternion.identity);
        newPlatform.GetComponent<Rigidbody>().velocity = new Vector3(-GameState.GameSpeed * texSpeedToVelocityMultiplier, 0f, 0f);
    }

    void PreparePlatform()
    {
        //slow current velocity down while holding button, like car brakes
        rb.drag *= 2f;
        rb.useGravity = false;
        rb.maxAngularVelocity = Mathf.Infinity;
        rb.angularDrag = 0f;
    }

    float bounceThresh = 2f;
    

    void Jump(float direction)
    {
        if (powerTimer < 0.3f)
        {
            if (direction == 1f && rb.velocity.y < -bounceThresh)
                Bounce(direction);
            else if (direction == -1f && rb.velocity.y > bounceThresh)
                Bounce(direction);
            else
                rb.velocity = rb.velocity + new Vector3(0f, direction * JumpPower * calculatePowerMultiplier());
        }
        else
            rb.velocity = rb.velocity + new Vector3(0f, direction * JumpPower * calculatePowerMultiplier());

        Physics.gravity = new Vector3(0f, -direction * GravityPower, 0f);
        rb.drag /= 2f;
        rb.useGravity = true;
    }

    void Bounce(float direction)
    {
        rb.velocity = -rb.velocity * 0.95f;
        rb.velocity += new Vector3(0f, direction * JumpPower * calculatePowerMultiplier());
    }

    float calculatePowerMultiplier()
    {
        return (1f + powerTimer);
    }
}
