﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class BlurControl : MonoBehaviour
{
    CameraMotionBlur blur;
    Vector3 blurScale;

    void Awake()
    {
        blur = GetComponent<CameraMotionBlur>();
        GameState.OnNormalMotion += Stop;
        GameState.OnSlowMotion += SlowMo;
        GameState.OnFastMotion += HighSpeed;
        blur.enabled = false;
    }

    public void SlowMo()
    {
        StopAllCoroutines();
        StartCoroutine(slowMo());
    }

    IEnumerator slowMo()
    {
        blur.enabled = true;
        blurScale.x = 0f;
        while (true)
        {
            blurScale.x = (Mathf.Sin(2f * Time.timeSinceLevelLoad))*2f;
            blur.previewScale = blurScale;
            yield return null;
        }
    }

    public void HighSpeed()
    {
        StopAllCoroutines();
        StartCoroutine(hiSpeed());
    }

    IEnumerator hiSpeed()
    {
        blur.enabled = true;
        while (true)
        {
            blurScale.x = 1f + Mathf.Sin(Time.timeSinceLevelLoad);
            blur.previewScale = blurScale;
            yield return null;
        }
    }

    public void Stop()
    {
        blur.enabled = false;
    }
}
