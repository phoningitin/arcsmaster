﻿using UnityEngine;
using System.Collections.Generic;

public class UIControl : MonoBehaviour
{
    [System.Serializable] public class UIState
    {
        public string name;
        public GameObject container;
    }
    public List<UIState> states;

    void Awake()
    {
        GameState.OnFail += OnDefeat;
        GameState.OnWin += OnVictory;
        GameState.OnPause += OnPaused;
        GameState.OnResume += OnResumed;

        OnResumed();
    }

    void OnResumed()
    {
        foreach (UIState state in states)
            state.container.SetActive(state.name == "Play");
    }

    void OnPaused()
    {
        foreach(UIState state in states)
            state.container.SetActive(state.name == "Pause");
    }

    void OnVictory()
    {
        foreach (UIState state in states)
            state.container.SetActive(state.name == "Win");
    }

    void OnDefeat()
    {
        foreach (UIState state in states)
            state.container.SetActive(state.name == "Lose");
    }
}
