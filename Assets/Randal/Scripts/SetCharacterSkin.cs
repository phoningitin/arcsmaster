﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SetCharacterSkin : MonoBehaviour
{
    [System.Serializable] public class CharSkin
    {
        public string name;
        public Sprite sprite;
    }
    public List<CharSkin> skins;
    public List<Image> imagesToChange;
    public List<SpriteRenderer> spritesToChange;
    void Awake()
    {
        CharSkin usingSkin = skins.Find(skin => skin.name == PlayerPrefs.GetString("SKIN", "Default"));
        if(GetComponent<SpriteRenderer>())
            GetComponent<SpriteRenderer>().sprite = usingSkin.sprite;
        foreach (SpriteRenderer sr in spritesToChange)
            sr.sprite = usingSkin.sprite;
        if (GetComponent<Image>())
            GetComponent<Image>().sprite = usingSkin.sprite;
        foreach (Image img in imagesToChange)
            img.sprite = usingSkin.sprite;
    }
    public void SetSkin(string skinName)
    {
        PlayerPrefs.SetString("SKIN", skinName);
        PlayerPrefs.Save();
    }
}
