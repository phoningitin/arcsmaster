﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnObstacles : MonoBehaviour
{
    [System.Serializable] public class ObstacleNode
    {
        public string name;
        public float probability;
    }

    public List<ObstacleNode> obstacles;
    public float defaultGenerationCheckTime;
    Bounds genArea;

	public void Start()
    {
        _inst = this;
        genArea = GetComponent<BoxCollider>().bounds;
        //StartCoroutine(generateLoop());
    }

    IEnumerator generateLoop()
    {
        while(true)
        {
            for (int i = 0; i < obstacles.Count; i++)
            {
                if(Random.Range(0f,1f) < obstacles[i].probability)
                    generate(obstacles[i].name);
            }
            yield return new WaitForSeconds(defaultGenerationCheckTime);
        }
    }

    void generate(string obstacleName)
    {
        Vector3 generationPosition = Utilities.RandomVector(genArea.min, genArea.max);
        PooledObjects.Instantiate(obstacleName, generationPosition, Quaternion.identity);
    }
    private static SpawnObstacles _inst;

    public static SpawnObstacles getInst() { return _inst; }

    public float TomatoNoSpace = .2f;
    public static void spawnTomato()
    {
        //Spawn a tomato around the most recently spawned note but not on the ghost trail please
        Vector3 generationPosition = Utilities.RandomVector(getInst().genArea.min, getInst().genArea.max);
        TrailNote tn = CreateNotes.getInst().getPrevNote();
        if (tn != null) {
            float p = tn.transform.position.y;
            bool spawnBelow = true, spawnAbove = true;
            if (Mathf.Abs(p - getInst().genArea.min.y) < getInst().TomatoNoSpace) spawnBelow = false;
            if (Mathf.Abs(p - getInst().genArea.max.y) < getInst().TomatoNoSpace) spawnAbove = false;
            float nsp = getInst().TomatoNoSpace;
            bool choose = (spawnAbove && spawnBelow);
            bool chosen = (Random.Range(0f,1f)<.5f);
            Debug.Log(chosen);
            if ((spawnAbove && !spawnBelow) || (choose && chosen))
            {
                generationPosition = new Vector3(getInst().genArea.center.x,Random.Range(p + nsp,getInst().genArea.max.y), getInst().genArea.center.z);
                PooledObjects.Instantiate("Tomato", generationPosition, Quaternion.identity);
                Debug.Log("Spawn Over");
            } else if ((!spawnAbove && spawnBelow)|| (choose && !chosen))
            {
                generationPosition = new Vector3(getInst().genArea.center.x, Random.Range(getInst().genArea.min.y, p - nsp), getInst().genArea.center.z);
                PooledObjects.Instantiate("Tomato", generationPosition, Quaternion.identity);
                Debug.Log("Spawn Under");
            } else
            {
                Debug.Log("Spawn Nothing: " + spawnAbove + "/" + spawnBelow);//do nothing, no room?    
            }

           
        }
    }

    public static void spawnCowbell(TrailNote t)
    {
        //Put a Bonus cowbell on behind a cool note.
        Vector3 generationPosition = new Vector3(t.transform.position.x, t.transform.position.y, getInst().genArea.center.z);//Utilities.RandomVector(getInst().genArea.min, getInst().genArea.max);
        GameObject o = PooledObjects.Instantiate("Cowbell", generationPosition, Quaternion.identity);
        o.GetComponent<Explode>().IsCowBell = true;
    }
}
