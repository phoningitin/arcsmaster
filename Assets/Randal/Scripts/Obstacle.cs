﻿using UnityEngine;
using System.Collections;

public class Obstacle : Scrollable
{
    public bool IsReady { get { return _isReady; } private set { _isReady = value; } }
    bool _isReady = true;

    public void SetReady()
    {
        IsReady = true;
    }

    public void SetUnready()
    {
        IsReady = false;
    }
}
