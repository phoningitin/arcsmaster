﻿using UnityEngine;
using System.Collections;

public class LoseGame : MonoBehaviour
{
    void OnCollisionEnter(Collision c)
    {
        if(c.collider.gameObject.tag == "Player")
        {
            GameState.OnFail();
        }
    }
}
