﻿using UnityEngine;
using System.Collections;

public class LoadOnClick : MonoBehaviour {

	public void loadScene(int scene)
    {
        Application.LoadLevel(scene);
        Time.timeScale = 1f;
    }

    public Animator MenuCharAnimator;
    public void hoverOver(bool h)
    {
        MenuCharAnimator.SetBool("Hovering",h);
    }
}
