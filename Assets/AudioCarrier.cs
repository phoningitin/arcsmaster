﻿using UnityEngine;
using System.Collections;

public class AudioCarrier : MonoBehaviour {

    // Use this for initialization
    public AudioClip[] SplatSounds;
    public AudioSource SplatSource;
    public AudioClip[] ComboChordSubtleSounds, ComboChordMajorSounds;
    public AudioSource ChordSource;
    public AudioClip[] SlowMoSounds;
    public AudioSource SlowMoSource;
    public AudioClip[] DisChordSounds;
    public AudioClip ComboBreakChord1, ComboBreakChord2;
    public AudioSource DisChordSource;
    public AudioSource CowbellSound;
    public AudioClip AudienceBOOOO,AudienceSlowClap, AudienceLightApplause, AudienceBigApplause,AudienceRoar;
    public AudioSource AudienceSource;
    public bool BigComboMade = false;
    public static AudioCarrier _inst;
    public static AudioCarrier getInst() { return _inst; }

    void Start()
    {
        _inst = this;
    }
    void OnAwake() {
        _inst = this;
        BigComboMade = false;
    }
	
	public void playSplatSound()
    {
        SplatSource.PlayOneShot(SplatSounds[Random.Range(0,SplatSounds.Length-1)],2f);
    }
    public void playChordSubtle()
    {
        AudioClip cl = ComboChordSubtleSounds[0];
        switch (GameState.GetInst().GetDifficulty())
        {
            case GameState.Difficulty.Easy:{cl = ComboChordSubtleSounds[0]; break;}
            case GameState.Difficulty.Normal: { cl = ComboChordSubtleSounds[1]; break; }
            case GameState.Difficulty.Hard: { cl = ComboChordSubtleSounds[2]; break; }
            case GameState.Difficulty.Literal: { cl = ComboChordSubtleSounds[Random.Range(0, ComboChordSubtleSounds.Length - 1)]; break; }
        }
        ChordSource.clip = cl;
        ChordSource.PlayOneShot(cl,2f);
        BigComboMade = true;
    }
    public void playChordMajor(int count)
    {
        AudioClip cl = ComboChordSubtleSounds[0];
        switch (GameState.GetInst().GetDifficulty())
        {
            case GameState.Difficulty.Easy: { cl = ComboChordMajorSounds[0]; break; }
            case GameState.Difficulty.Normal: { cl = ComboChordMajorSounds[1]; break; }
            case GameState.Difficulty.Hard: { cl = ComboChordMajorSounds[2]; break; }
            case GameState.Difficulty.Literal: { cl = ComboChordMajorSounds[Random.Range(0, ComboChordMajorSounds.Length - 1)]; break; }
        }
        ChordSource.clip = cl;
        if (count > 1)
        {
            float span = .3f;
            for (int i = 0; i < count; i++)
            ChordSource.PlayDelayed(span* ((float)((float)i/(float)(count-1))));
        } else {
            ChordSource.Play();
        }
        
        BigComboMade = true;
    }
    public void playMissChord(int combobroken)
    {
        //ChordSource.Stop();
        if (BigComboMade)
        {
            BigComboMade = false;
            DisChordSource.pitch = 1f;
            DisChordSource.PlayOneShot((combobroken >= 25) ? ComboBreakChord2 : ComboBreakChord1, .5f);


        } else
        {
            if (combobroken <= 2) {
                float sp = GameState.GetInst().getCurrentSpeed();
            if (sp >= .75f) {
                DisChordSource.pitch = 1f;
            } else
            {
                DisChordSource.pitch = Mathf.Lerp(.25f, 1f, sp / .75f);
            }

            DisChordSource.PlayOneShot(DisChordSounds[Random.Range(0, DisChordSounds.Length - 1)], .5f);
            }
        }

        
    }
    public void playSlowMoSound()
    {
        SlowMoSource.clip = SlowMoSounds[Random.Range(0, SlowMoSounds.Length - 1)];
        SlowMoSource.Play();
    }

    public void stopSlowMoSound()
    {
        SlowMoSource.Stop();
        

    }
    public void playCowbellSound()
    {
        CowbellSound.Stop();
        CowbellSound.volume = 1f;
        CowbellSound.pitch = 1f;
        CowbellSound.Play();
    }
    public void playCowbellSound(float pitch)
    {
        CowbellSound.volume = .5f;
        CowbellSound.Stop();
        CowbellSound.pitch = pitch;
        CowbellSound.Play();
    }

    public void playAudienceSound(int cheerlevel)
    {
        AudioClip cl = AudienceSlowClap;
        float volm = 1f;
        
        switch (cheerlevel)
        {
            case 0: { cl = AudienceBOOOO; volm = 1f; break; }
            case 1: { cl = AudienceSlowClap; volm = 1f; break; }
            case 2: { cl = AudienceLightApplause; volm = 1f; break; }
            case 3: { cl = AudienceBigApplause; volm = 1f; break; }
            case 4: { cl = AudienceRoar; volm = 1f; break; }
        }
        AudienceSource.PlayOneShot(cl,volm);
        
    }
}
